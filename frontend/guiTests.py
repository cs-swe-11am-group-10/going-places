import os
import sys

if __name__ == "__main__":
    dir = os.path.dirname(__file__)

    passing = True
    for testName in [
        "navbarTests.py",
        "citiesTests.py",
        "hotelsTests.py",
        "eventsTests.py",
    ]:
        testPath = os.path.join(dir, "gui_tests", testName)
        testCode = os.system("python " + testPath)
        passing = passing and (testCode == 0)

    if not passing:
        print("NOT PASSING")
        sys.exit(1)
