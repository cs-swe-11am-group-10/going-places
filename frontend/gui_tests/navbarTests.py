import unittest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

import sys

url = "https://www.goingtoplaces.me/"


# Framework from https://gitlab.com/forbesye/fitsbits/
class TestNavbar(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1040")
        chrome_options.add_argument("--headless")  # comment for visualization
        chrome_options.add_argument("--no-sandbox")  # needed for docker
        chrome_options.add_argument("--disable-dev-shm-usage")  # needed for docker
        cls.driver = webdriver.Chrome(
            ChromeDriverManager().install(), options=chrome_options
        )
        cls.driver.implicitly_wait(10)  # wait 10 seconds when trying to find elements
        cls.driver.get(url)

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testHome(self):
        self.driver.find_element(by=By.LINK_TEXT, value="CITIES").click()
        assert self.driver.current_url != url

        self.driver.find_element(by=By.LINK_TEXT, value="HOME").click()
        assert self.driver.current_url == url

        element = self.driver.find_element(by=By.CLASS_NAME, value="title")
        assert element.text == "GOING PLACES"

    def testCities(self):
        self.driver.find_element(by=By.LINK_TEXT, value="CITIES").click()
        assert self.driver.current_url == "https://www.goingtoplaces.me/cities"

        element = self.driver.find_element(by=By.TAG_NAME, value="h3")
        assert element.text == "CITIES"

        self.driver.back()
        assert self.driver.current_url == url

    def testHotels(self):
        self.driver.find_element(by=By.LINK_TEXT, value="HOTELS").click()
        assert self.driver.current_url == "https://www.goingtoplaces.me/hotels"

        element = self.driver.find_element(by=By.TAG_NAME, value="h3")
        assert element.text == "HOTELS"

        self.driver.back()
        assert self.driver.current_url == url

    def testEvents(self):
        self.driver.find_element(by=By.LINK_TEXT, value="EVENTS").click()
        assert self.driver.current_url == "https://www.goingtoplaces.me/events"

        element = self.driver.find_element(by=By.TAG_NAME, value="h3")
        assert element.text == "EVENTS"

        self.driver.back()
        assert self.driver.current_url == url

    def testAbout(self):
        self.driver.find_element(by=By.LINK_TEXT, value="ABOUT").click()
        assert self.driver.current_url == "https://www.goingtoplaces.me/about"

        element = self.driver.find_element(by=By.CLASS_NAME, value="pageTitle")
        assert element.text == "About Us"

        self.driver.back()
        assert self.driver.current_url == url

    def testSearch(self):
        self.driver.find_element(by=By.LINK_TEXT, value="SEARCH").click()
        assert self.driver.current_url == "https://www.goingtoplaces.me/search"

        element = self.driver.find_element(by=By.CLASS_NAME, value="pageTitle")
        assert element.text == "Sitewide Search"

        self.driver.back()
        assert self.driver.current_url == url

    def testVisualizations(self):
        self.driver.find_element(by=By.LINK_TEXT, value="VISUALIZATIONS").click()
        assert self.driver.current_url == "https://www.goingtoplaces.me/visualizations"

        element = self.driver.find_element(by=By.CLASS_NAME, value="pageTitle")
        assert element.text == "Visualizations"

        self.driver.back()
        assert self.driver.current_url == url

    def testProviderVisualizations(self):
        self.driver.find_element(
            by=By.LINK_TEXT, value="PROVIDER VISUALIZATIONS"
        ).click()
        assert (
            self.driver.current_url
            == "https://www.goingtoplaces.me/provider-visualizations"
        )

        element = self.driver.find_element(by=By.CLASS_NAME, value="pageTitle")
        assert element.text == "Provider Visualizations"

        self.driver.back()
        assert self.driver.current_url == url


if __name__ == "__main__":
    unittest.main()
