import unittest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

import sys
import time

url = "https://www.goingtoplaces.me/cities"


# Framework from https://gitlab.com/forbesye/fitsbits/
class TestCities(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1040")
        chrome_options.add_argument("--headless")  # comment for visualization
        chrome_options.add_argument("--no-sandbox")  # needed for docker
        chrome_options.add_argument("--disable-dev-shm-usage")  # needed for docker
        cls.driver = webdriver.Chrome(
            ChromeDriverManager().install(), options=chrome_options
        )
        cls.driver.implicitly_wait(10)  # wait 10 seconds when trying to find elements
        cls.driver.get(url)

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testCityInstance(self):
        self.driver.find_element(
            by=By.XPATH, value="/html/body/div/div/div/div/div[2]/table/tbody/tr[1]"
        ).click()
        assert self.driver.current_url[: len(url)] == url
        id = self.driver.current_url[len(url) + 1 :]
        assert id.isnumeric()

        self.driver.find_element(by=By.LINK_TEXT, value="CITIES").click()
        assert self.driver.current_url == url

    def testPagination(self):

        # Store name of first row
        self.driver.get(url)
        time.sleep(3)
        name = self.driver.find_element(
            by=By.XPATH,
            value="/html/body/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]",
        ).text

        # Go to second page and check url plus name of first row
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(3)
        self.driver.find_element(by=By.LINK_TEXT, value="2").click()
        time.sleep(3)
        assert self.driver.current_url == url + "?page=2"
        assert (
            self.driver.find_element(
                by=By.XPATH,
                value="/html/body/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]",
            ).text
            != name
        )

        # Go back to first page and check url plus name of first row
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(3)
        self.driver.find_element(by=By.LINK_TEXT, value="1").click()
        time.sleep(3)
        assert self.driver.current_url == url + "?page=1"
        assert (
            self.driver.find_element(
                by=By.XPATH,
                value="/html/body/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]",
            ).text
            == name
        )

    def testSorting(self):
        self.driver.get(url)
        time.sleep(3)

        # Sort by name descending and check url plus names of first five rows
        self.driver.find_element(
            by=By.XPATH,
            value="/html/body/div/div/div/div/div[2]/table/thead/tr/th[1]/button[2]",
        ).click()
        time.sleep(3)
        assert self.driver.current_url == url + "?page=1&sort=name-D"
        prev_name = self.driver.find_element(
            by=By.XPATH,
            value="/html/body/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]",
        ).text
        for i in range(2, 6):
            next_name = self.driver.find_element(
                by=By.XPATH,
                value="/html/body/div/div/div/div/div[2]/table/tbody/tr["
                + str(i)
                + "]/td[1]",
            ).text
            assert next_name <= prev_name
            prev_name = next_name

        # Sort by name ascending and check url plus names of first five rows
        self.driver.find_element(
            by=By.XPATH,
            value="/html/body/div/div/div/div/div[2]/table/thead/tr/th[1]/button[1]",
        ).click()
        time.sleep(3)
        assert self.driver.current_url == url + "?page=1&sort=name-A"
        prev_name = self.driver.find_element(
            by=By.XPATH,
            value="/html/body/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]",
        ).text
        for i in range(2, 6):
            next_name = self.driver.find_element(
                by=By.XPATH,
                value="/html/body/div/div/div/div/div[2]/table/tbody/tr["
                + str(i)
                + "]/td[1]",
            ).text
            assert next_name >= prev_name
            prev_name = next_name

    def testFiltering(self):
        self.driver.get(url)
        time.sleep(3)

        # Filter by state of California and check url plus states of first five rows
        self.driver.find_element(
            by=By.XPATH,
            value="/html/body/div/div/div/div/div[2]/table/thead/tr/th[2]/div/button",
        ).click()
        self.driver.find_element(
            by=By.XPATH,
            value="/html/body/div/div/div/div/div[2]/table/thead/tr/th[2]/div/div/a[5]",
        ).click()
        time.sleep(3)
        assert self.driver.current_url == url + "?page=1&state=California"
        for i in range(1, 6):
            assert (
                self.driver.find_element(
                    by=By.XPATH,
                    value="/html/body/div/div/div/div/div[2]/table/tbody/tr["
                    + str(i)
                    + "]/td[2]",
                ).text
                == "California"
            )

    def testSearching(self):
        self.driver.get(url)
        time.sleep(3)

        # Search Kentucky and check url plus states of first two rows
        self.driver.find_element(
            by=By.XPATH, value="/html/body/div/div/div/div/div[1]/div[1]/input"
        ).send_keys("Kentucky")
        self.driver.find_element(
            by=By.XPATH, value="/html/body/div/div/div/div/div[1]/div[1]/input"
        ).send_keys(Keys.RETURN)
        time.sleep(3)
        assert self.driver.current_url == url + "?page=1&search=Kentucky"
        for i in range(1, 3):
            assert (
                self.driver.find_element(
                    by=By.XPATH,
                    value="/html/body/div/div/div/div/div[2]/table/tbody/tr["
                    + str(i)
                    + "]/td[2]",
                ).text
                == "Kentucky"
            )


if __name__ == "__main__":
    unittest.main()
