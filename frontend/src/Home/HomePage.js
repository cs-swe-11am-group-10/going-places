import { Card } from 'react-bootstrap';
import "./HomePage.css";
import YoutubeEmbed from "./YoutubeEmbed"
import logo from "./Splash.png";
import "bootstrap/dist/css/bootstrap.min.css";


function HomePage() {
    return (
        <>
            <div style={{ backgroundImage: `url(${logo})`, backgroundSize: "cover", height: "100vh", width: "100%" }}>
                <Card.Text className= "title"> GOING PLACES </Card.Text>
                <Card.Text className="subheading"> Find your home away from home and travel to exotic locations with exciting events</Card.Text>
            </div>
            <div>
                <YoutubeEmbed embedId="brc0CdO-Sp4" />
            </div>
        </>
    );
}

export default HomePage;