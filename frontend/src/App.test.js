import React from "react";
import * as renderer  from "react-test-renderer";
import {BrowserRouter as Router} from "react-router-dom";
import formatNumber from "./components/formatNumber";
import formatPrice from "./components/formatPrice";
import Navigation from "./Navigation";
import HomePage from "./Home/HomePage";
import { memberInfo, MemberCard, toolInfo, ToolCard, apiInfo, ApiCard, CityCard, HotelCard, EventCard, ResultsCard } from "./components/Cards";
import image from "./person.png";
import {kindFilters, sortText } from "./components/FilterOptions";
import Paginate from "./components/Pagination";

//Test 1
test("Format Number", () => {
    expect(formatNumber(1001.005)).toBe("1,001.01");
    expect(formatNumber(1001.0049999)).toBe("1,001");
    expect(formatNumber(1001.995)).toBe("1,002");
    expect(formatNumber(1001.1)).toBe("1,001.1");
    expect(formatNumber(123456789)).toBe("123,456,789");
});

// Test 2
test("Format Price", () => {
    expect(formatPrice(1001.005)).toBe("$1,001.01");
    expect(formatPrice(1001.0049999)).toBe("$1,001.00");
    expect(formatPrice(1001.995)).toBe("$1,002.00");
    expect(formatPrice(1001.1)).toBe("$1,001.10");
    expect(formatPrice(123456789)).toBe("$123,456,789.00");
    expect(formatPrice(-1)).toBe("Not Listed");
});

// Test 3
test("Home Page", () => {
    const tree = renderer.create(<HomePage />).toJSON();
    expect(tree).toMatchSnapshot();
});

//Test 4
test("Filtering Options", () => {
    expect(kindFilters[1]).toBe("natural");
    expect(kindFilters[2]).toBe("cultural");
    expect(kindFilters[3]).toBe("historic");
});

//Test 5
test("Sorting Options", () => {
    expect(sortText[0]).toBe("A-Z");
    expect(sortText[1]).toBe("Z-A");
});

// Test 6
test("Navbar", () => {
    const tree = renderer.create(<Navigation />).toJSON();
    expect(tree).toMatchSnapshot();
});

// Test 7
test("Member Card", () => {
    const testMember = new memberInfo("name", "username", "issueUsername", 0, 0, image, "bio", 0, "role", "phaseLeader");
    const tree = renderer.create(<MemberCard data={testMember} />).toJSON();
    expect(tree).toMatchSnapshot();
});

// Test 8
test("Tool Card", () => {
    const testTool = new memberInfo("https://www.google.com", image, "alt", "title", "text");
    const tree = renderer.create(<ToolCard data={testTool} />).toJSON();
    expect(tree).toMatchSnapshot();
});

// Test 9
test("Api Card", () => {
    const testApi = new apiInfo("https://www.google.com", {}, image, "alt", "title", "text");
    const tree = renderer.create(<ApiCard data={testApi} />).toJSON();
    expect(tree).toMatchSnapshot();
});

//Test 10
test("City Card", () => {
    const testCity = {id:0, image_url:"https://reactjs.org/logo-og.png", name:"name", budget_value:0, safety_value:0}
    const tree = renderer.create(<Router> <CityCard data={testCity} /> </Router>).toJSON();
    expect(tree).toMatchSnapshot();
});

//Test 11
test("Hotel Card", () => {
    const testHotel = {id:0, image_url:"https://reactjs.org/logo-og.png", name:"name", star_rating:0, price:0}
    const tree = renderer.create(<Router> <HotelCard data={testHotel} /> </Router>).toJSON();
    expect(tree).toMatchSnapshot();
});

//Test 12
test("Event Card", () => {
    const testEvent = {id:0, image_url:"https://reactjs.org/logo-og.png", name:"name", rating:0}
    const tree = renderer.create(<Router> <EventCard data={testEvent} /> </Router>).toJSON();
    expect(tree).toMatchSnapshot();
});

//Test 13
test("Results Card", () => {
    const tree = renderer.create(<Router> <ResultsCard model={"model"} search={"search"} /> </Router>).toJSON();
    expect(tree).toMatchSnapshot();
});

// Test 14
test("Pagination", () => {
    const tree = renderer.create(<Paginate />).toJSON();
    expect(tree).toMatchSnapshot();
});
