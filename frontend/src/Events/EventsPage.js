import React, { useState, useEffect } from "react";
import { FormControl, Button, Col, Row, Card } from 'react-bootstrap';
import axios from "axios";
import { useQueryParams, StringParam, NumberParam, withDefault } from 'use-query-params';

import formatNumber from "../components/formatNumber";
import "./EventsStyle.css";
import { kindFilters, stateFilters, sortText } from "../components/FilterOptions";

import ModelTable from '../components/ModelTable';
import Paginate from '../components/Pagination';

function EventsPage() {
    const [query, setQuery] = useQueryParams({
        search: withDefault(StringParam, undefined),
        state_name: withDefault(StringParam, undefined),
        primary_kind: withDefault(StringParam, undefined),
        sort: withDefault(StringParam, undefined),
        page: withDefault(NumberParam, 1)
    });

    const [numResults, setNumResults] = useState(0);
    const [tableData, setTableData] = useState([]);
    const [input, setInput] = useState(query.search);

    // functions for Pagination
    function setPage(page) {
        setQuery({'page':page}, 'pushIn');
    }

    function setSort(sort) {
        setQuery({...query, ...{'sort':sort, 'page':1}}, 'push');
    }

    function setFilter(key, value) {
        setQuery({...query, ...{[key]:value, 'page':1}}, 'push');
    }

    const columns = [
        { heading: 'Name', accessor: 'name',  filters: kindFilters,
             both: false, isNumber:false, isList:false, sortOptions: sortText},
        { heading: 'City', accessor: 'city_name', filters: kindFilters,
             both: false, isNumber:false, isList:false, sortOptions: sortText},
        { heading: 'State', accessor: 'state_name',  filters: stateFilters,
            both: true, isNumber:false, isList:false, sortOptions: sortText},
        { heading: 'Features', accessor: 'kinds', filters: stateFilters,
            both: false, isNumber:false, isList:true, sortOptions: sortText},
        { heading: 'Type', accessor: 'primary_kind', filters: kindFilters,
            both: true, isNumber:false, isList:false, sortOptions: sortText}
    ];

     function formatList(x) {
         return x.replaceAll("_", " ");
     }

    useEffect(() => {
      (async () => {

            // build queries in order of search, filters, sort, page
            let allQueryStr = "https://api.goingtoplaces.me/events?";
            if(typeof(query.search) !== 'undefined') {
                allQueryStr += "search=" + query.search + "&";
            }
            if(typeof(query.state_name) !== 'undefined') {
                allQueryStr += "state_name=" + query.state_name + "&";
            }
            if(typeof(query.primary_kind) !== 'undefined') {
                allQueryStr += "primary_kind=" + query.primary_kind + "&";
            }
            if(typeof(query.sort) !== 'undefined') {
                allQueryStr += "sort=" + query.sort + "&";
            }
            const all = await axios(allQueryStr);
            setNumResults(all.data.length);

            let queryStr = allQueryStr + "page=" + query.page;
            const result = await axios(queryStr);
            result.data.forEach(function(x) {
              columns.forEach(function(column) {
                  if(column.isNumber) {
                      x[column.accessor] = formatNumber(x[column.accessor]);
                  }
                  if (column.isList){
                    x[column.accessor] = formatList(x[column.accessor]);
                  }
              });
            });
            setTableData(result.data);
      })();
    }, [query])

    // functions for search submission
    function handleChange(event) {
        setInput(event.target.value);
    }

    function submit() {
        if(input === "") {
            setQuery({...query, ...{'search':undefined, 'page':1}}, 'push');
        }
        else {
            setQuery({...query, ...{'search':input, 'page':1}}, 'push');
        }
    }

    function onKeyUp(event) {
        if (event.key === "Enter") {
            submit();
        }
    }

    return (
        <div className='info-box'>
        <Card body className='main-card'>
            <h3> EVENTS </h3>
            <h5> Total Instances: {numResults} </h5>

            <Row className="justify-content-md-center">
                    <Col className="col-9" style={{marginRight:0, paddingRight:0}}>
                        <FormControl value={input} onChange={handleChange} onKeyUp={onKeyUp} />
                    </Col>
                    <Col className="col-1" style={{marginLeft:0, paddingLeft:0}}>
                        <Button variant="outline-secondary" onClick={() => submit()}> Search </Button>
                    </Col>
            </Row>

            <div className='inner-flex'>
                <ModelTable data={tableData} columns={columns} setSort={setSort} setFilter={setFilter} search={query.search} queryFilters={{state_name:query.state_name, primary_kind:query.primary_kind}}/>
            </div>

            <div className='inner-flex'>
             <Paginate totalItems={numResults} itemsPerPage={10} paginate={setPage} page={query.page} />
            </div>

       </Card>
       </div>
    )
}

export default EventsPage;
