import formatNumber from "../components/formatNumber";
import React, { useState, useEffect } from "react";
import axios from "axios";
import "../PageStyle.css";
import "../Forecast/ForecastFormat.css";
import "../InfoStyle.css";
import { useParams } from "react-router-dom";
import { Col, Row, Card, Table } from 'react-bootstrap';
import { HotelCard } from  "../components/Cards";
import { checkForUnavailableData } from "../dataCheck.js";



function EventInstance() {
    const { id } = useParams();
    
    // get info
    const [eventData, setEventData] = useState([]);
    const [hotelsData, setHotelssData] = useState([]);
    const [weatherData, setWeatherData] = useState([]);
    
    useEffect(() => {
    	(async () => {
    		// hotel api call
      		const eventResult = await axios("https://api.goingtoplaces.me/events/" + id);
        	setEventData(eventResult.data);

			// events api call
        	const hotelResult = await axios("https://api.goingtoplaces.me/hotels/city/" + eventResult.data.event_city_id);
        	setHotelssData(hotelResult.data); 
        	
        	// weather api call
        	const weatherResult = await axios("https://api.openweathermap.org/data/2.5/weather?lat="+eventResult.data.latitude+"&lon="+eventResult.data.longitude+"&appid=24e52896891c076e351b8dbf4138da15&units=imperial");
        	setWeatherData(weatherResult.data);
    	})();
  	}, []);
  	
  	// make sure data is set before displaying page
	if (weatherData == undefined || weatherData.main == undefined || weatherData.weather == undefined)
		return null;
		
	// make sure event data is set
	if (eventData == undefined)
		return null;
		
	// check if event info is available; if not, set a default value
	let aboutEvent = eventData.info_text == "" ? "Information unavailable." : eventData.info_text;

    return (
        <div class="pagePadding">
            {/* Title  */}
            <Row>
                <Col>
                    <Card.Text className= "pageTitle"> {eventData.name} </Card.Text>
                </Col>
            </Row>
            
            {/* Image */}
            <Row>
            	<Col>
            		<div class="imgBorder">
        			<img src={eventData.image_url} width="100%"
        				height="auto">
        			</img>
        			</div>
        		</Col>	
        	</Row>
        	
        	{/* Map and Weather */}
        	<Row>
        		<Col>
					<div class="mapFormat">
           			<iframe src={"https://www.google.com/maps/embed/v1/view?key=AIzaSyDM7FKGcp--ZacdBRQ6C45Wtn7qDJFgcrg&center="+eventData.latitude+","+eventData.longitude+"&zoom=16"}
        				width="100%"
        				height="300px" >
           			</iframe>
           			</div>
				</Col>
				
				<Col>
					<div class="forecast">
						<p class="label"> Current weather in </p>
           				<p class="areaName"> {weatherData.name} </p>
           				<p class="temp"> {weatherData.main.temp}&deg;F </p>
           				<div class="iconDisplay">
           					<img class="icon" src={"http://openweathermap.org/img/wn/"+weatherData.weather[0].icon+"@2x.png"}></img>
           					<p class="description"> {weatherData.weather[0].main} </p>
           				</div>
           				<hr class="lineBreak"></hr>
           				<p class="otherInfo"> Feels Like: {weatherData.main.feels_like}&deg;F </p>
           				<p class="otherInfo"> Humidity: {weatherData.main.humidity}% </p>
           				<p class="otherInfo"> Wind Speed: {weatherData.wind.speed} mph </p>
           				<p class="otherInfo"> Visibility: {weatherData.visibility} meters </p>
           			</div>
           		</Col>
           	</Row>
           	
           	{/* Event Information  */}
           	<Row>
           		<Col>
           			<div class="infoBox">
           				<h2 class="infoHeading">Information</h2>
           				<hr></hr>
           				<body class="infoBody"> {aboutEvent} </body>
           			</div>
           		</Col>
           	</Row>

            {/* Attribute table  */}
            <Table bordered>
                <tbody>
                <tr>
                    <td class="infoTableLabel"> City </td>
                    <td> <a href={`/cities/${eventData.event_city_id}`}>{eventData.city_name}</a> </td>
                </tr>
                <tr>
                    <td class="infoTableLabel"> Kind </td>
                    <td> {eventData.primary_kind} </td>
                </tr>
                <tr>
                    <td class="infoTableLabel"> Rating </td>
                    <td> {formatNumber(eventData.rating)}/5 </td>
                </tr>
                </tbody>
            </Table>

            {/* Hotels  */}
            <Row>
                <Col>
                    <Card.Text className= "pageTitle"> Nearby Hotels </Card.Text>
                </Col>
            </Row>
            <Row>
                {hotelsData.map((data) => (
                    <Col>
                        <HotelCard data={data} />
                    </Col>
                  ))}
            </Row>
        </div>
    );
}

export default EventInstance;
