import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import { BrowserRouter as Router, Routes, Route, useNavigate, useLocation } from "react-router-dom";
import { QueryParamProvider } from 'use-query-params';

import Navigation from "./Navigation";
import Home from "./Home/HomePage";
import CitiesPage from "./Cities/CitiesPage";
import CityInstance from "./Cities/CityInstance";
import HotelsPage from "./Hotels/HotelsPage";
import HotelInstance from "./Hotels/HotelInstance";
import EventsPage from "./Events/EventsPage";
import EventInstance from "./Events/EventInstance";
import AboutPage from "./About/AboutPage";
import SearchPage from "./Search/SearchPage";
import VisualizationsPage from "./Visualizations/VisualizationsPage";
import ProviderVisualizationsPage from "./ProviderVisualizations/ProviderVisualizationsPage";

const React = require('react');

const RouteAdapter = ({ children }) => {
 const navigate = useNavigate();
 const location = useLocation();

 const adaptedHistory = React.useMemo(
   () => ({
     replace(location) {
       navigate(location, { replace: true, state: location.state });
     },
     push(location) {
       navigate(location, { replace: false, state: location.state });
     },
   }),
   [navigate]
 );
 return children({ history: adaptedHistory, location });
};

function App() {
    return (
        <>
            <Navigation/>
            <Router>
                <QueryParamProvider ReactRouterRoute={RouteAdapter}>
                    <Routes>
                        <Route path="/" element={<Home/>} />
                        <Route path="cities" element={<CitiesPage/>} />
                        <Route path="cities/:id" element={<CityInstance/>} />
                        <Route path="hotels" element={<HotelsPage/>}/>
                        <Route path="hotels/:id" element={<HotelInstance/>}/>
                        <Route path="about" element={<AboutPage/>} />
                        <Route path="events" element={<EventsPage/>}/>
                        <Route path="events/:id" element={<EventInstance/>}/>
                        <Route path="search" element={<SearchPage/>}/>
                        <Route path="visualizations" element={<VisualizationsPage/>}/>
                        <Route path="provider-visualizations" element={<ProviderVisualizationsPage/>}/>
                    </Routes>
                </QueryParamProvider>
            </Router>
        </>
    );
}

export default App;
