import React, { useState, useEffect } from "react";
import { Container, Col, Row, Card } from 'react-bootstrap';
import axios from "axios";
import convert from 'convert-zip-to-gps';
import * as d3 from "d3";
import * as topojson from "topojson-client";

import BubbleChart from "../components/BubbleChart";
import SpikeMap from "../components/SpikeMap";
import PieChart from "../components/PieChart";
import formatNumber from "../components/formatNumber";

import "../PageStyle.css";

import austinCounties from './AustinCounties.json'; // https://data.austintexas.gov/Locations-and-Maps/Counties/u6wx-p5c8
import texasZipCodes from './TexasZipCodes.json'; // https://github.com/storiesofsolidarity/us-data/blob/gh-pages/geography/zcta/Texas.topo.json
const nation = topojson.feature(austinCounties, austinCounties.objects.Counties)
const statemesh = topojson.mesh(texasZipCodes, texasZipCodes.objects["Texas.geo"], (a, b) => a !== b)

function HousingVisualization(props) {
    const width = props.width;
    const height = props.height;

    const [data, setData] = useState([]);

    useEffect(() => {
      (async () => {
            const result = await axios("https://api.affordaustin.me/api/housing");
            const counts = new Map();
            result.data.forEach(x => {
                if(counts.has(x.unit_type)) {
                    counts.set(x.unit_type, counts.get(x.unit_type)+1);
                }
                else {
                    counts.set(x.unit_type, 1);
                }
            });
            setData(Array.from(counts, ([unit_type, count]) => ({ unit_type, count })));
      })();
    }, [])

    return PieChart(data, {name: d => d.unit_type,
                           value: d => d.count,
                           width: width,
                           height: height});
}

function ChildCareVisualization(props) {
    const width = props.width;
    const height = props.height;

    const [data, setData] = useState([]);

    useEffect(() => {
      (async () => {
            const result = await axios("https://api.affordaustin.me/api/childcare");
            setData(result.data);
      })();
    }, [])


    return BubbleChart(data, {name: d => d.operation_name+"\n"+formatNumber(d.total_capacity),
                              value: d => d.total_capacity,
                              title: d => d.operation_name+"\n"+formatNumber(d.total_capacity),
                              width: width,
                              height: height});

}

function JobsVisualization(props) {
    const width = props.width;
    const height = props.height;

    const [data, setData] = useState([]);

    useEffect(() => {
      (async () => {
            const result = await axios("https://api.affordaustin.me/api/jobs");
            const counts = new Map();
            result.data.forEach(x => {
                if(counts.has(x.zip_code)) {
                    counts.set(x.zip_code, counts.get(x.zip_code)+1);
                }
                else {
                    counts.set(x.zip_code, 1);
                }
            });
            setData(Array.from(counts, ([zip_code, count]) => ({ zip_code, count })));
      })();
    }, [])

    return SpikeMap(data, {value: d => d.count,
                           title: d => d.zip_code+"\n"+formatNumber(d.count),
                           position: d => convert.zipConvert(d.zip_code).split(',').map(x => parseFloat(x)).reverse(),
                           features: nation,
                           borders: statemesh,
                           projection: d3.geoAlbersUsa().fitExtent([[0, 0], [960, 500]], nation),
                           width: width,
                           height: height});
}

function ProviderVisualizationsPage() {
    return (
        <div class="pagePadding">
            <Container>
                <Card.Text className= "pageTitle"> Provider Visualizations </Card.Text>

                <Card.Text className= "pageTitle" style={{ fontSize: "35px" }}> Housing Unit Type Pie Chart </Card.Text>
                <Row className="justify-content-md-center" >
                    <Col style={{maxWidth:"960px"}}>
                        <HousingVisualization width={960} height={500}/>
                    </Col>
                </Row>

                <Card.Text className= "pageTitle" style={{ fontSize: "35px", marginBottom:"0px", paddingBottom:"0px" }}> Child Care Capacity Bubble Chart </Card.Text>
                <Row className="justify-content-md-center" >
                    <Col style={{maxWidth:"960px"}}>
                        <ChildCareVisualization width={960} height={500}/>
                    </Col>
                </Row>

                <Card.Text className= "pageTitle" style={{ fontSize: "35px" }}> Jobs by Zip Code Spike Map </Card.Text>
                <Row className="justify-content-md-center" >
                    <Col style={{maxWidth:"960px"}}>
                        <JobsVisualization width={960} height={500}/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default ProviderVisualizationsPage;