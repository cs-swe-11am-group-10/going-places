// https://stackabuse.com/how-to-format-number-as-currency-string-in-javascript/
function formatPrice(x) {
    if(x === -1) {
        return "Not Listed";
    }
    else {
        const dollarUSLocale = Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', useGrouping: true });
        return dollarUSLocale.format(x);
    }
}

export default formatPrice;
