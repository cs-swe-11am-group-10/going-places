import { Card } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";
import Highlighter from "react-highlight-words";

import formatNumber from "./formatNumber";
import formatPrice from "./formatPrice";

import "./CardsStyle.css";

class memberInfo {
    constructor (name, username, issueUsername, image, bio, unitTests, role, phaseLeader) {
        this.name = name;
        this.username = username;
        this.issueUsername = issueUsername;
        this.commits = 0;
        this.issues = 0;
        this.image = image;
        this.bio = bio;
        this.unitTests = unitTests;
        this.role = role;
        this.phaseLeader = phaseLeader;
    }
}

function MemberCard(props) {
    const data = props.data;

    return (
        <Card className="cardStyle" style={{ width: '14.5rem' }}>
            <Card.Img src={data.image} alt="sweta" />
            <Card.Body>
                <Card.Title>{data.name}</Card.Title>
                <Card.Text>{data.role}</Card.Text>
                <Card.Text> {data.bio} </Card.Text>
                <Card.Text> {data.phaseLeader} </Card.Text>
                <Card.Text> Commits: {data.commits}</Card.Text>
                <Card.Text> Issues: {data.issues}  </Card.Text>
                <Card.Text> Unit Tests: {data.unitTests}</Card.Text>
            </Card.Body>
        </Card>
    );
}

class toolInfo {
    constructor (href, image, alt, title, text) {
        this.href = href;
        this.image = image;
        this.alt = alt;
        this.title = title;
        this.text = text;
    }
}

function ToolCard(props) {
    const data = props.data;

    return (
        <Card className="toolsStyle" onClick={e => window.open(data.href)} style={{ width: '12rem', cursor: 'pointer' }}>
            <Card.Img src={data.image} alt={data.alt} />
            <Card.Body>
                <Card.Title>{data.title}</Card.Title>
                <Card.Text> {data.text}</Card.Text>
            </Card.Body>
        </Card>
    );
}

class apiInfo {
    constructor (href, posStyle, image, alt, title, text) {
        this.href = href;
        this.posStyle = posStyle;
        this.image = image;
        this.alt = alt;
        this.title = title;
        this.text = text;
    }
}

function ApiCard(props) {
    const data = props.data;

    return (
        <Card className="toolsStyle" onClick={e => window.open(data.href)} style={{...{width: '15rem', cursor: 'pointer'},...data.posStyle}}>
            <Card.Img src={data.image} alt={data.alt} />
            <Card.Body>
                <Card.Title>{data.title}</Card.Title>
                <Card.Text> {data.text}</Card.Text>
            </Card.Body>
        </Card>
    );
}

function CityCard(props) {
    const data = props.data;
    const search = props.hasOwnProperty("search") ? props.search : "";
    const searchWords = search.split(" ");

    const navigate = useNavigate();
    function goTo() {
        navigate(`/cities/${data.id}`);
    }

    return (
        <Card className="cardStyle" onClick={() => goTo()} style={{ width: '15rem', cursor: 'pointer' }}>
            <Card.Img src={data.image_url} alt="city image" />
            <Card.Body>
                <Card.Title>
                    <Highlighter searchWords={searchWords} textToHighlight={data.name} />
                </Card.Title>
                <Card.Text>
                    {"Budget Rating: " + formatNumber(data.budget_value)}
                </Card.Text>
                <Card.Text>
                    {"Safety Rating: " + formatNumber(data.safety_value)}
                </Card.Text>
            </Card.Body>
        </Card>
    );
}

function HotelCard(props) {
    const data = props.data;
    const search = props.hasOwnProperty("search") ? props.search : "";
    const searchWords = search.split(" ");

    const navigate = useNavigate();
    function goTo() {
        navigate(`/hotels/${data.id}`);
    }

    return (
        <Card className="cardStyle" onClick={() => goTo()} style={{ width: '15rem', cursor: 'pointer' }}>
            <Card.Img src={data.image_url} alt="hotel image" />
            <Card.Body>
                <Card.Title>
                    <Highlighter searchWords={searchWords} textToHighlight={data.name} />
                </Card.Title>
                <Card.Text>
                    {"Stars: " + formatNumber(data.star_rating)}
                </Card.Text>
                <Card.Text>
                    {"Price: " + formatPrice(data.price)}
                </Card.Text>
            </Card.Body>
        </Card>
    );
}

function EventCard(props) {
    const data = props.data;
    const search = props.hasOwnProperty("search") ? props.search : "";
    const searchWords = search.split(" ");

    const navigate = useNavigate();
    function goTo() {
        navigate(`/events/${data.id}`);
    }

    return (
        <Card className="cardStyle" onClick={() => goTo()} style={{ width: '15rem', cursor: 'pointer' }}>
            <Card.Img src={data.image_url} alt="event image" />
            <Card.Body>
                <Card.Title>
                    <Highlighter searchWords={searchWords} textToHighlight={data.name} />
                </Card.Title>
                <Card.Text>
                    {"Rating: " + formatNumber(data.rating)}
                </Card.Text>
            </Card.Body>
        </Card>
    );
}

function ResultsCard(props) {
    const model = props.model;
    const search = props.search;

    const navigate = useNavigate();
    function goTo() {
        navigate(`/${model}?search=${search}`);
    }

    return (
        <Card className="cardStyle" onClick={() => goTo()} style={{ width: '20rem', cursor: 'pointer' }}>
            <Card.Body>
                <Card.Title> {"Click me for more " + model} </Card.Title>
            </Card.Body>
        </Card>
    );
}

export { memberInfo, MemberCard, toolInfo, ToolCard, apiInfo, ApiCard, CityCard, HotelCard, EventCard, ResultsCard};//, InstanceCard };