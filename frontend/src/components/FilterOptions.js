const kindFilters =
 [ "ALL",
   "natural" ,
  "cultural",
  "historic"]

const stateFilters =
[ "ALL",
  "Alabama",
"Arizona",
"Arkansas",
"California",
"Colorado",
"Connecticut",
"Florida",
"Georgia",
"Idaho",
"Illinois",
"Indiana",
"Iowa",
"Kansas",
"Kentucky",
"Louisiana",
"Maryland",
"Massachusetts",
"Michigan",
"Missouri",
"Nebraska",
"Nevada",
"New Jersey",
"New Mexico",
"New York",
"North Carolina",
"Ohio",
"Oklahoma",
"Oregon",
"Pennsylvania",
"Rhode Island",
"South Carolina",
"Tennessee",
"Texas",
"Utah",
"Vermont",
"Virginia",
"Washington",
"Wisconsin"
]

const sortText =
 ["A-Z",
  "Z-A"];

  const sortNum =
  ["Low-High",
   "High-Low"];

   const sortNumBudget =
   ["Affordable-Expensive",
   "Expensive-Affordable"];
export {
  kindFilters,
  stateFilters,
  sortText,
   sortNum,
   sortNumBudget
}

