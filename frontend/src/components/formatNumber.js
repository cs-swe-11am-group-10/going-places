// Return number as formatted string, i.e. commas and rounded to 2 decimals
// https://stackoverflow.com/questions/11832914/how-to-round-to-at-most-2-decimal-places-if-necessary
// https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
function formatNumber(x) {
    x = +(Math.round(x + "e+" + 2)  + "e-" + 2);
    return x.toLocaleString();
}

export default formatNumber;