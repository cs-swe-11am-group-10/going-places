import React from 'react'
import { Pagination } from 'react-bootstrap'

const Paginate = ({ totalItems, itemsPerPage, paginate, page }) => {
    const currentPage = page;

    // Paginate prop is used so that the appropriate page will be fetched in the model pages.
    const changePage = (num) => {
        paginate(num)
    }

    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(totalItems / itemsPerPage); i++) {
        pageNumbers.push(i)
    }

    return (
        <div>
            {/* Using the pagination component from react bootstrap. */}
            <Pagination size='sm'>
                <Pagination.First onClick={() => changePage(1)}  />
                <Pagination.Prev onClick={() => changePage(Math.max(1, currentPage - 1))} />
                {Array.from({ length: pageNumbers.length }).map((_, idx) => (
                    <Pagination.Item key={pageNumbers[idx]} active={pageNumbers[idx] === currentPage} onClick={() => changePage(pageNumbers[idx])}>
                        {pageNumbers[idx]}
                    </Pagination.Item>
                ))}
                <Pagination.Next onClick={() => changePage(Math.min(pageNumbers.length, currentPage + 1))} />
                <Pagination.Last  onClick={() => changePage(pageNumbers.length)} />
            </Pagination>
        </div>
    )
}

export default Paginate
