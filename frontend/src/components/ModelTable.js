import './ModelTable.css'
import { useNavigate } from "react-router-dom";
import { ArrowUp, ArrowDown } from "react-bootstrap-icons";
import {Button, Dropdown, ButtonGroup} from 'react-bootstrap';
import Highlighter from "react-highlight-words";
import e from 'cors';

function ModelTable(props) {
    const data = props.data;
    const columns = props.columns;
    const setSort = props.setSort;
    const setFilter = props.setFilter;
    const search = props.search;
    const searchWords = typeof(search) !== 'undefined' ? search.split(" ") : [];
    const queryFilters = props.queryFilters;

    function changeFilter(column, value) {
        if (value == "ALL") {
            setFilter(column.accessor, undefined);
        }
        else {
            setFilter(column.accessor, value);
        }
    }

    function changeSort(column, num) {
        if (num === 1) {
            setSort(column.accessor + "-A")
        }
        if (num === 2) {
            setSort(column.accessor + "-D")
        }
    }

    const FilterComponent = ({column}) => (
        <Dropdown as={ButtonGroup}>
            <Dropdown.Toggle id='dropdown-basic' className="dd-outer" size="sm" style={{ backgroundColor: '#6D0E8E'}} >
                {typeof(queryFilters[column.accessor]) !== 'undefined' ? queryFilters[column.accessor] : 'ALL'}
            </Dropdown.Toggle>

            <Dropdown.Menu className="dd-inner">
                {column.filters.map((filter, i) =>
                    <Dropdown.Item className = "options" onClick ={() => changeFilter(column, filter)} >
                        {filter}
                    </Dropdown.Item>)}
            </Dropdown.Menu>
        </Dropdown>

    )

    const SortComponent = ({column}) => (
        <> <Button size="xs" onClick={() => changeSort(column, 1)} variant="info"> <ArrowUp /> {column.sortOptions[0]}</Button> {" "}
        <Button size="xs" onClick={() => changeSort(column, 2)} variant="info"> <ArrowDown />  {column.sortOptions[1]}</Button> </>
    )

  return (
    <table>
        <thead>
            <tr >
            {columns.map((column, index) =>
                <th> {column.heading + " "}
                {column.both ? <><FilterComponent column={column} />  <SortComponent column={column} /> </> :
                    <SortComponent column={column}/> }
                </th>
                 )}
            </tr>
        </thead>
        <tbody>
            {data.map((item, index) => <TableRow item={item} columns={columns} searchWords={searchWords} />)}
        </tbody>
    </table>
  );
}

function TableRow(props) {
    const item = props.item;
    const columns = props.columns;
    const searchWords = props.searchWords;

    const navigate = useNavigate();
    function goToInstance(id) {
        navigate(id.toString());
    }

    return (
        <tr className='TableRow' onClick={() => goToInstance(item['id'])} style={{ cursor: 'pointer' }}>
            {columns.map((column, index) => (<td> <Highlighter searchWords={searchWords} textToHighlight={item[column.accessor]} /> </td>))}
        </tr>
    );
}

export default ModelTable;

