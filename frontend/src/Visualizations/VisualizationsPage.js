import React, { useState, useEffect } from "react";
import { Container, Col, Row, Card } from 'react-bootstrap';
import axios from "axios";
import * as d3 from "d3";
import * as topojson from "topojson-client";

import BubbleChart from "../components/BubbleChart";
import SpikeMap from "../components/SpikeMap";
import PieChart from "../components/PieChart";
import formatNumber from "../components/formatNumber";
import formatPrice from "../components/formatPrice";

import "../PageStyle.css";

import us from './counties-10m.json'; // https://github.com/topojson/us-atlas
const nation = topojson.feature(us, us.objects.nation)
const statemesh = topojson.mesh(us, us.objects.states, (a, b) => a !== b)

function CityVisualization(props) {
    const width = props.width;
    const height = props.height;

    const [data, setData] = useState([]);

    useEffect(() => {
      (async () => {
            const result = await axios("https://api.goingtoplaces.me/cities");
            setData(result.data);
      })();
    }, [])

    return BubbleChart(data, {name: d => d.name+"\n"+formatNumber(d.population),
                              value: d => d.population,
                              title: d => d.name+"\n"+formatNumber(d.population),
                              width: width,
                              height: height});
}

function HotelVisualization(props) {
    const width = props.width;
    const height = props.height;

    const [data, setData] = useState([]);

    useEffect(() => {
      (async () => {
            const result = await axios("https://api.goingtoplaces.me/hotels");
            setData(result.data);
      })();
    }, [])

    return SpikeMap(data, {value: d => d.price === -1 ? 0 : d.price,
                           title: d => d.name+"\n"+formatPrice(d.price),
                           position: d => [d.longitude, d.latitude],
                           features: nation,
                           borders: statemesh,
                           projection: d3.geoAlbersUsa(),
                           width: width,
                           height: height});
}

function EventVisualization(props) {
    const width = props.width;
    const height = props.height;

    const [data, setData] = useState([]);

    useEffect(() => {
      (async () => {
            const result = await axios("https://api.goingtoplaces.me/events");
            const counts = new Map()
            result.data.forEach(x => {
                if(counts.has(x.primary_kind)) {
                    counts.set(x.primary_kind, counts.get(x.primary_kind)+1);
                }
                else {
                    counts.set(x.primary_kind, 0);
                }
            });
            setData(Array.from(counts, ([primary_kind, count]) => ({ primary_kind, count })));
      })();
    }, [])

    return PieChart(data, {name: d => d.primary_kind,
                           value: d => d.count,
                           width: width,
                           height: height});
}

function VisualizationsPage() {
    return (
        <div class="pagePadding">
            <Container>
                 <Card.Text className= "pageTitle"> Visualizations </Card.Text>

                <Card.Text className= "pageTitle" style={{ fontSize: "35px", marginBottom:"0px", paddingBottom:"0px" }}> City Population Bubble Chart </Card.Text>
                <Row className="justify-content-md-center" >
                    <Col style={{maxWidth:"960px"}}>
                        <CityVisualization width={960} height={500}/>
                    </Col>
                </Row>

                <Card.Text className= "pageTitle" style={{ fontSize: "35px"}}> Hotel Price Spike Map </Card.Text>
                <Row className="justify-content-md-center">
                    <Col style={{maxWidth:"960px"}}>
                        <HotelVisualization width={960} height={500}/>
                    </Col>
                </Row>

                <Card.Text className= "pageTitle" style={{ fontSize: "35px"}}> Event Type Pie Chart </Card.Text>
                <Row className="justify-content-md-center">
                    <Col style={{maxWidth:"960px"}}>
                        <EventVisualization width={960} height={500}/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default VisualizationsPage;