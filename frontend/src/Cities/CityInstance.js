import { useParams } from "react-router-dom";
import { Col, Row, Card, Table } from 'react-bootstrap';
import { HotelCard, EventCard } from "../components/Cards";
import formatNumber from "../components/formatNumber";
import { checkForUnavailableData } from "../dataCheck.js";

import React, { useState, useEffect } from "react";
import axios from "axios";

import "../PageStyle.css";
import "../Forecast/ForecastFormat.css";
import "../InfoStyle.css";

function CityInstance() {
    const { id } = useParams();
    
    //TODO set api keys for maps and weather as a separate variable
    
    // get info
    const [cityData, setCityData] = useState([]);
    const [hotelsData, setHotelsData] = useState([]);
    const [eventsData, setEventsData] = useState([]);
    const [weatherData, setWeatherData] = useState([]);

    
    useEffect(() => {
    	(async () => {
    		// city api call
      		const cityResult = await axios("https://api.goingtoplaces.me/cities/" + id);
        	setCityData(cityResult.data);
    	
    		// hotels api call
      		const hotelsResult = await axios("https://api.goingtoplaces.me/hotels/city/" + id);
        	setHotelsData(hotelsResult.data);

			// events api call
        	const eventResult = await axios("https://api.goingtoplaces.me/events/city/" + id);
        	setEventsData(eventResult.data);
        	
        	// weather api call
        	const weatherResult = await axios("https://api.openweathermap.org/data/2.5/weather?lat="+cityResult.data.latitude+"&lon="+cityResult.data.longitude+"&appid=24e52896891c076e351b8dbf4138da15&units=imperial");
        	setWeatherData(weatherResult.data);
    	})();
  	}, []);
  	
  	// make sure data is set before displaying page
	if (weatherData == undefined || weatherData.main == undefined || weatherData.weather == undefined)
		return null;
		
	// make sure city data is set
	if (cityData == undefined)
		return null;
		
	// parse out commas from information lists
	var knownFor = cityData.known_for.split(',');
	
	// if info lists are empty, set the first (and only) entry in each parsed array to "Data Unavailable"
	checkForUnavailableData(knownFor)
		
	// check if city info is available; if not, set a default value
	let aboutCity = cityData.info_text == "" ? "Information unavailable." : cityData.info_text;
	

    return (
        <div class="pagePadding">
            {/* Title  */}
            <Row>
                <Col>
                    <Card.Text className= "pageTitle"> {cityData.long_name} </Card.Text>
                </Col>
            </Row>
            {/* Image */}
            <Row>
            	<Col>
            		<div class="imgBorder">
        			<img src={cityData.image_url} width="100%"
        				height="auto">
        			</img>
        			</div>
        		</Col>	
        	</Row>
        	
        	{/* Map and Weather */}
        	<Row>
        		<Col>
					<div class="mapFormat">
           			<iframe src={"https://www.google.com/maps/embed/v1/view?key=AIzaSyDM7FKGcp--ZacdBRQ6C45Wtn7qDJFgcrg&center="+cityData.latitude+","+cityData.longitude+"&zoom=10"}
        				width="100%"
        				height="300px" >
           			</iframe>
           			</div>
           		</Col>
           		
           		<Col>
					<div class="forecast">
						<p class="label"> Current weather in </p>
           				<p class="areaName"> {weatherData.name} </p>
           				<p class="temp"> {weatherData.main.temp}&deg;F </p>
           				<div class="iconDisplay">
           					<img class="icon" src={"http://openweathermap.org/img/wn/"+weatherData.weather[0].icon+"@2x.png"}></img>
           					<p class="description"> {weatherData.weather[0].main} </p>
           				</div>
           				<hr class="lineBreak"></hr>
           				<p class="otherInfo"> Feels Like: {weatherData.main.feels_like}&deg;F </p>
           				<p class="otherInfo"> Humidity: {weatherData.main.humidity}% </p>
           				<p class="otherInfo"> Wind Speed: {weatherData.wind.speed} mph </p>
           				<p class="otherInfo"> Visibility: {weatherData.visibility} meters </p>
           			</div>
           		</Col>
           	</Row>
           	
           	{/* City Information  */}
           	<Row>
           		<Col>
           			<div class="infoBox">
           				<h2 class="infoHeading">About {cityData.name}</h2>
           				<hr></hr>
           				<body class="infoBody"> {aboutCity} </body>
           			</div>
           		</Col>
           	</Row>

            {/* Attribute table  */}
            <Row>
            	<Col>
					<Table bordered>
						<tbody>
						<tr>
							<td class="infoTableLabel"> Continent </td>
							<td> {cityData.continent} </td>
						</tr>
						<tr>
							<td class="infoTableLabel"> Country </td>
							<td> {cityData.country} </td>
						</tr>
						<tr>
							<td class="infoTableLabel"> Latitude </td>
							<td> {formatNumber(cityData.latitude)} </td>
						</tr>
						<tr>
							<td class="infoTableLabel"> Longitude </td>
							<td> {formatNumber(cityData.longitude)} </td>
						</tr>
						<tr>
							<td class="infoTableLabel"> Population </td>
							<td> {formatNumber(cityData.population)} </td>
						</tr>
						<tr>
							<td class="infoTableLabel"> Budget Rating </td>
							<td> {formatNumber(cityData.budget_value)}/10 </td>
						</tr>
						<tr>
							<td class="infoTableLabel"> Safety Rating </td>
							<td> {formatNumber(cityData.safety_value)}/10 </td>
						</tr>
						<tr>
							<td class="infoTableLabel"> Average Rating </td>
							<td> {formatNumber(cityData.avg_rating)}/10 </td>
						</tr>
						</tbody>
					</Table>
				</Col>
				
				<Col>
					<Table bordered>
					<tbody>
						<tr>
							<td class="infoTableLabel"> Known For </td>
							<td>
								<ul>
                    				{knownFor.map((data) => (
                    					<li>{data}</li>
                    				))}
                    			</ul>
							</td>
						</tr>
					</tbody>
					<tbody>
						<tr>
							<td class="linksInfoTableLabel"> External Links </td>
							<td> 
								<a href={cityData.airbnb_link} target="_blank"> airbnb </a>
								<p/>
								<a href={cityData.google_events_link} target="_blank"> Google Events </a>
								<p/>
								<a href={cityData.walk_score_link} target="_blank"> Walk Score </a>
								<p/>
								<a href={cityData.wikipedia_link} target="_blank"> Wikipedia </a>
							</td>
						</tr>
						</tbody>
					</Table>
				</Col>
			</Row>	

            {/* Hotels  */}
            <Row>
                <Col>
                    <Card.Text className= "pageTitle"> Hotels </Card.Text>
                </Col>
            </Row>
            <Row>
                {hotelsData.map((data) => (
                    <Col>
                        <HotelCard data={data} />
                    </Col>
                  ))}
            </Row>

            {/* Events  */}
            <Row>
                <Col>
                    <Card.Text className= "pageTitle"> Events </Card.Text>
                </Col>
            </Row>
            <Row>
                {eventsData.map((data) => (
                    <Col>
                        <EventCard data={data} />
                    </Col>
                  ))}
            </Row>
        </div>
    );
}

export default CityInstance;
