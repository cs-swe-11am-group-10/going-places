import React, {useState, useEffect} from "react";
import { FormControl, Button, Col, Row, Card } from 'react-bootstrap';
import axios from "axios";
import { useQueryParams, StringParam,withDefault } from 'use-query-params';

import { CityCard, HotelCard, EventCard, ResultsCard } from "../components/Cards"

import "../PageStyle.css";

function SearchPage() {
    const [query, setQuery] = useQueryParams({
        search: withDefault(StringParam, undefined),
    });

    const [input, setInput] = useState(query.search);
    const [cityQueryData, setCityQueryData] = useState([]);
    const [cityNumResults, setCityNumResults] = useState(0);
    const [hotelQueryData, setHotelQueryData] = useState([]);
    const [hotelNumResults, setHotelNumResults] = useState(0);
    const [eventQueryData, setEventQueryData] = useState([]);
    const [eventNumResults, setEventNumResults] = useState(0);

    useEffect(() => {
        if(typeof(query.search) === 'undefined') {
            setCityQueryData([]);
            setCityNumResults(0);
            setHotelQueryData([]);
            setHotelNumResults(0);
            setEventQueryData([]);
            setEventNumResults(0);
        }
        else {
            (async () => {
                const cityAll = await axios("https://api.goingtoplaces.me/cities?search=" + query.search);
                setCityNumResults(cityAll.data.length);
                const cityResult = await axios("https://api.goingtoplaces.me/cities?search=" + query.search + "&page=1");
                setCityQueryData(cityResult.data);
                const hotelAll = await axios("https://api.goingtoplaces.me/hotels?search=" + query.search);
                setHotelNumResults(hotelAll.data.length);
                const hotelResult = await axios("https://api.goingtoplaces.me/hotels?search=" + query.search + "&page=1");
                setHotelQueryData(hotelResult.data);
                const eventAll = await axios("https://api.goingtoplaces.me/events?search=" + query.search);
                setEventNumResults(eventAll.data.length);
                const eventResult = await axios("https://api.goingtoplaces.me/events?search=" + query.search + "&page=1");
                setEventQueryData(eventResult.data);
            })();
        }
    }, [query])

    function handleChange(event) {
        setInput(event.target.value);
    }

    function submit() {
        if(input == "") {
            setQuery({'search':undefined});
        }
        else {
            setQuery({'search':input});
        }
    }

    function onKeyUp(event) {
        if (event.key === "Enter") {
            submit();
        }
    }

    let results;
    if(typeof(query.search) === 'undefined') {
        results = (<></>);
    }
    else {
        results = (
            <>
                <Row>
                    <Col>
                        <Card.Text className= "pageTitle"> {cityNumResults+" City Results for \""+query.search+"\""} </Card.Text>
                    </Col>
                </Row>
                <Row>
                    {cityQueryData.map((data) => (
                        <Col>
                            <CityCard data={data} search={query.search} />
                        </Col>
                      ))}
                </Row>
                <Row>
                    {cityNumResults <= 10 ? (<></>) : (
                        <Col>
                            <ResultsCard model={"cities"} search={query.search} />
                        </Col>
                    )}
                </Row>
                <Row>
                    <Col>
                        <Card.Text className= "pageTitle"> {hotelNumResults+" Hotel Results for \""+query.search+"\""} </Card.Text>
                    </Col>
                </Row>
                <Row>
                    {hotelQueryData.map((data) => (
                        <Col>
                            <HotelCard data={data} search={query.search} />
                        </Col>
                      ))}
                </Row>
                <Row>
                    {hotelNumResults <= 10 ? (<></>) : (
                        <Col>
                            <ResultsCard model={"hotels"} search={query.search} />
                        </Col>
                    )}
                </Row>
                <Row>
                    <Col>
                        <Card.Text className= "pageTitle"> {eventNumResults+" Event Results for \""+query.search+"\""} </Card.Text>
                    </Col>
                </Row>
                <Row>
                    {eventQueryData.map((data) => (
                        <Col>
                            <EventCard data={data} search={query.search} />
                        </Col>
                      ))}
                </Row>
                <Row>
                    {eventNumResults <= 10 ? (<></>) : (
                        <Col>
                            <ResultsCard model={"events"} search={query.search} />
                        </Col>
                    )}
                </Row>
            </>
        );
    }

    return (
        <div class="pagePadding">

             {/* Sitewide Search Heading  */}
             <Row>
                 <Col>
                    <Card.Text className= "pageTitle"> Sitewide Search </Card.Text>
                 </Col>
            </Row>

            <Row className="justify-content-md-center">
                    <Col className="col-9" style={{marginRight:0, paddingRight:0}}>
                        <FormControl value={input} onChange={handleChange} onKeyUp={onKeyUp} />
                    </Col>
                    <Col className="col-1" style={{marginLeft:0, paddingLeft:0}}>
                        <Button variant="outline-secondary" onClick={() => submit()}> Search </Button>
                    </Col>
            </Row>

            {results}
        </div>
    );
}

export default SearchPage;
