import formatNumber from "./components/formatNumber";
import formatPrice from "./components/formatPrice";

// check some sent in data to see if it has data to be displayed
// if yes, ignore
// if no, either alter incoming object or return value with default value for when data is "unavailable"
export function checkForUnavailableData(data)
{
	if (Array.isArray(data))
	{
		if (data.length == 1 && data[0] == "")
			data[0] = "Information Unavailable"
	}
}
