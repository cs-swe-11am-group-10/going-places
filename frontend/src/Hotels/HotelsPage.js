import React, { useState, useEffect } from "react";
import { FormControl, Button, Col, Row, Card } from 'react-bootstrap';
import axios from "axios";
import { useQueryParams, StringParam, NumberParam, withDefault } from 'use-query-params';

import formatNumber from "../components/formatNumber";
import "./HotelStyle.css";

import ModelTable from '../components/ModelTable';
import Paginate from '../components/Pagination';
import { stateFilters, kindFilters, sortText, sortNum} from "../components/FilterOptions";

function HotelsPage() {
    const [query, setQuery] = useQueryParams({
        search: withDefault(StringParam, undefined),
        province_name: withDefault(StringParam, undefined),
        sort: withDefault(StringParam, undefined),
        page: withDefault(NumberParam, 1)
    });

    const [numResults, setNumResults] = useState(0);
    const [tableData, setTableData] = useState([]);
    const [input, setInput] = useState(query.search);

    // functions for Pagination
    function setPage(page) {
        setQuery({'page':page}, 'pushIn');
    }

    function setSort(sort) {
        setQuery({...query, ...{'sort':sort, 'page':1}}, 'push');
    }

    function setFilter(key, value) {
        setQuery({...query, ...{[key]:value, 'page':1}}, 'push');
    }
    

    const columns = [
        { heading: 'Name', accessor: 'name',  filters: kindFilters , both: false,
             isNumber:false, sortOptions: sortText},
        { heading: 'City', accessor: 'city_name', filters: stateFilters , both: false, isNumber:false, sortOptions: sortText},
        { heading: 'State', accessor: 'province_name',  filters: stateFilters, both: true, isNumber:false, sortOptions: sortText},
        { heading: 'Star Rating (out of 5)', accessor: 'star_rating', filters: kindFilters , both: false, isNumber:true , sortOptions: sortNum},
        { heading: 'Guest Rating (out of 10)', accessor: 'guest_rating', filters: kindFilters, both: false, isNumber:true , sortOptions: sortNum}
    ]

    useEffect(() => {
      (async () => {

            // build queries in order of search, filters, sort, page
            let allQueryStr = "https://api.goingtoplaces.me/hotels?";
            if(typeof(query.search) !== 'undefined') {
                allQueryStr += "search=" + query.search + "&";
            }
            if(typeof(query.province_name) !== 'undefined') {
                allQueryStr += "province_name=" + query.province_name + "&";
            }
            if(typeof(query.sort) !== 'undefined') {
                allQueryStr += "sort=" + query.sort + "&";
            }
            const all = await axios(allQueryStr);
            setNumResults(all.data.length);

            let queryStr = allQueryStr + "page=" + query.page;
            const result = await axios(queryStr);
            result.data.forEach(function(x) {
              columns.forEach(function(column) {
                  if(column.isNumber) {
                      x[column.accessor] = formatNumber(x[column.accessor]);
                  }
              });
            });
            setTableData(result.data);
      })();
    }, [query])

    // functions for search submission
    function handleChange(event) {
        setInput(event.target.value);
    }

    function submit() {
        if(input === "") {
            setQuery({...query, ...{'search':undefined, 'page':1}}, 'push');
        }
        else {
            setQuery({...query, ...{'search':input, 'page':1}}, 'push');
        }
    }

    function onKeyUp(event) {
        if (event.key === "Enter") {
            submit();
        }
    }

    return (
        <div className='info-box'>
        <Card body className='main-card'>
            <h3> HOTELS </h3>
            <h5> Total Instances: {numResults} </h5>

            <Row className="justify-content-md-center">
                    <Col className="col-9" style={{marginRight:0, paddingRight:0}}>
                        <FormControl value={input} onChange={handleChange} onKeyUp={onKeyUp} />
                    </Col>
                    <Col className="col-1" style={{marginLeft:0, paddingLeft:0}}>
                        <Button variant="outline-secondary" onClick={() => submit()}> Search </Button>
                    </Col>
            </Row>

            <div className='inner-flex'>
                <ModelTable data={tableData} columns={columns} setSort={setSort} setFilter={setFilter} search={query.search} queryFilters={{province_name:query.province_name}}/>
            </div>

            <div className='inner-flex'>
             <Paginate totalItems={numResults} itemsPerPage={10} paginate={setPage} page={query.page} />
            </div>

       </Card>
       </div>
    )
}

export default HotelsPage;
