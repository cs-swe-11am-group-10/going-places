import { useParams } from "react-router-dom";
import { Col, Row, Card, Table } from 'react-bootstrap';
import { EventCard } from "../components/Cards";
import { checkForUnavailableData } from "../dataCheck.js";

import formatNumber from "../components/formatNumber";
import formatPrice from "../components/formatPrice";

import React, { useState, useEffect } from "react";
import axios from "axios";

import "../PageStyle.css";
import "../Forecast/ForecastFormat.css";

function HotelInstance() {
    const { id } = useParams();
    
    // get info
    const [hotelData, setHotelData] = useState([]);
    const [eventsData, setEventsData] = useState([]);
    const [weatherData, setWeatherData] = useState([]);
    
    useEffect(() => {
    	(async () => {
    		// hotel api call
      		const hotelResult = await axios("https://api.goingtoplaces.me/hotels/" + id);
        	setHotelData(hotelResult.data);

			// events api call
        	const eventResult = await axios("https://api.goingtoplaces.me/events/city/" + hotelResult.data.hotel_city_id);
        	setEventsData(eventResult.data); 
        	
        	// weather api call
        	const weatherResult = await axios("https://api.openweathermap.org/data/2.5/weather?lat="+hotelResult.data.latitude+"&lon="+hotelResult.data.longitude+"&appid=24e52896891c076e351b8dbf4138da15&units=imperial");
        	setWeatherData(weatherResult.data);
    	})();
  	}, []);
  	
  	// make sure data is set before displaying page
	if (weatherData == undefined || weatherData.main == undefined || weatherData.weather == undefined)
		return null;
	if (hotelData == undefined)
		return null;
		
	// parse out commas from information lists
	var amenities = hotelData.main_amenities.split(',');
	var forFamilies = hotelData.for_families.split(',');
	var petsInfo = hotelData.pets.split(',');
	
	// if info lists are empty, set the first (and only) entry in each parsed array to "Data Unavailable"
	checkForUnavailableData(amenities)
	checkForUnavailableData(forFamilies)
	checkForUnavailableData(petsInfo)
	
    return (
        <div class="pagePadding">
            {/* Title  */}
            <Row>
                <Col>
                    <Card.Text className= "pageTitle"> {hotelData.name} </Card.Text>
                </Col>
            </Row>
            
            {/* Images */}
            <Row>
            	<Col>
            		<div class="imgBorder">
        			<img src={hotelData.image_url} width="100%"
        				height="auto">
        			</img>
        			</div>
        		</Col>	
        		
        		<Col>
        			<div class="imgBorder">
        			<img src={hotelData.room_image_url} width="100%"
        				height="auto">
        			</img>
        			</div>
        		</Col>	
        	</Row>
        	
        	{/* Map and Weather */}
        	<Row>
        		<Col>
					<div class="mapFormat">
           			<iframe src={"https://www.google.com/maps/embed/v1/view?key=AIzaSyDM7FKGcp--ZacdBRQ6C45Wtn7qDJFgcrg&center="+hotelData.latitude+","+hotelData.longitude+"&zoom=18"}
        				width="100%"
        				height="300px" >
           			</iframe>
           			</div>
           		</Col>
           		
           		<Col>
					<div class="forecast">
						<p class="label"> Current weather in </p>
           				<p class="areaName"> {weatherData.name} </p>
           				<p class="temp"> {weatherData.main.temp}&deg;F </p>
           				<div class="iconDisplay">
           					<img class="icon" src={"http://openweathermap.org/img/wn/"+weatherData.weather[0].icon+"@2x.png"}></img>
           					<p class="description"> {weatherData.weather[0].main} </p>
           				</div>
           				<hr class="lineBreak"></hr>
           				<p class="otherInfo"> Feels Like: {weatherData.main.feels_like}&deg;F </p>
           				<p class="otherInfo"> Humidity: {weatherData.main.humidity}% </p>
           				<p class="otherInfo"> Wind Speed: {weatherData.wind.speed} mph </p>
           				<p class="otherInfo"> Visibility: {weatherData.visibility} meters </p>
           			</div>
           		</Col>
           	</Row>	

            {/* Attribute table  */}
            <Table bordered class="infoTable">
                <tbody>
                <tr>
                    <td class="infoTableLabel"> Star Rating </td>
                    <td> {formatNumber(hotelData.star_rating)}/5 </td>
                </tr>
                <tr>
                    <td class="infoTableLabel"> Guest Rating </td>
                    <td> {formatNumber(hotelData.guest_rating)}/10 (from {formatNumber(hotelData.num_ratings)} ratings) </td>
                </tr>
                <tr>
                    <td class="infoTableLabel"> City </td>
                    <td> <a href={`/cities/${hotelData.hotel_city_id}`}>{hotelData.city_name}</a> </td>
                </tr>
                <tr>
                    <td class="infoTableLabel"> Main Amenities </td>
                    <td>
                    	<ul>
                    		{amenities.map((data) => (
                    			<li>{data}</li>
                    		))}
                    	</ul>
                    </td>
                </tr>
                <tr>
                    <td class="infoTableLabel"> For Families </td>
                    <td> 
                    	<ul>
                    		{forFamilies.map((data) => (
                    			<li>{data}</li>
                    		))}
                    	</ul>
                    </td>
                </tr>
                <tr>
                    <td class="infoTableLabel"> Pets </td>
                    <td> 
                    	<ul> 
                    		{petsInfo.map((data) => (
                    			<li>{data}</li>
                    		))}
                    	</ul> 
                    </td>
                </tr>
                <tr>
                    <td class="infoTableLabel"> Price </td>
                    <td> {formatPrice(hotelData.price)} </td>
                </tr>
                </tbody>
            </Table>
            
            {/* Events  */}
            <Row>
                <Col>
                    <Card.Text className= "pageTitle"> Nearby Events </Card.Text>
                </Col>
            </Row>
            <Row>
                {eventsData.map((data) => (
                    <Col>
                        <EventCard data={data} />
                    </Col>
                  ))}
            </Row>
			
        </div>
    );
}

export default HotelInstance;
