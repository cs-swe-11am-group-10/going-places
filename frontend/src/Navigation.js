import { Navbar, Nav, Container } from 'react-bootstrap';

const Navigation = () => {
    return (
        <Navbar style={{ backgroundColor: '#6D0E8E'}} expand="lg">
            <Container>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav" >
                <Nav className="me-auto">
                    <Nav.Link style={{ color: '#fff', fontFamily: 'Roboto', fontWeight: "bold"}} href="/">HOME</Nav.Link>
                    <Nav.Link style={{ color: '#fff', fontFamily: 'Roboto', fontWeight: "bold"}} href="/cities">CITIES</Nav.Link>
                    <Nav.Link style={{ color: '#fff', fontFamily: 'Roboto', fontWeight: "bold"}} href="/hotels"> HOTELS</Nav.Link>
                    <Nav.Link style={{ color: '#fff', fontFamily: 'Roboto', fontWeight: "bold"}} href="/events">EVENTS</Nav.Link>
                    <Nav.Link style={{ color: '#fff', fontFamily: 'Roboto', fontWeight: "bold"}} href="/about">ABOUT</Nav.Link>
                    <Nav.Link style={{ color: '#fff', fontFamily: 'Roboto', fontWeight: "bold"}} href="/search">SEARCH</Nav.Link>
                    <Nav.Link style={{ color: '#fff', fontFamily: 'Roboto', fontWeight: "bold"}} href="/visualizations">VISUALIZATIONS</Nav.Link>
                    <Nav.Link style={{ color: '#fff', fontFamily: 'Roboto', fontWeight: "bold"}} href="/provider-visualizations">PROVIDER VISUALIZATIONS</Nav.Link>
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>

    )
}

export default Navigation;