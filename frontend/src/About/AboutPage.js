import React, { useEffect, useState } from 'react';
import axios from "axios"
import { Col, Row, Container, Card } from 'react-bootstrap';
import { memberInfo, MemberCard, toolInfo, ToolCard, apiInfo, ApiCard } from "../components/Cards";

import sweta from "./sweta.png";
import mccray from "./mccray.png";
import joriann from "./joriann.jpg";
import james from "./james.png";
import shreya from "./shreya.png"
import reactLogo from "./logo512.png";
import discord from "./discord.png";
import figma from "./figma.png";
import canva from "./canva.png"
import gitlab from "./gitlab.png";
import postman from "./postman.png";
import aws from "./aws.png";
import roadgoat from "./roadgoat.png";
import hotels from "./hotelslogo.png";
import maps from "./maps.png";
import opentrip from "./opentrip.png";
import docker from "./docker.png"
import namecheap from "./namecheap.png"
import black from "./black.png"
import jest from "./jest.png"
import selenium from "./selenium.png"
import prettier from "./prettier.png"
import flask from "./flask.png"
import mysql from "./mysql.png"
import sqlalchemy from "./sqlalchemy.png"
import unittest from "./unittest.png"
import d3 from "./d3.png"
import openweather from "./openweather.jpeg"

import "./AboutPage.css";
import "../PageStyle.css";
import "../components/CardsStyle.css";

const allMemberInfo = [
    new memberInfo(
        "Sweta Ghose",
        "swetagh",
        "swetagh",
        sweta,
        "I am a junior CS major with a minor in communications studies and business at UT Austin from Cypress, Texas. I love watching sports despite not being very good at playing them and traveling!",
        5,
        "Frontend",
        "Phase 1 Leader"
    ),
    new memberInfo(
        "Joriann Bassi",
        "joriann-marie",
        "joriann-marie",
        joriann,
        "I'm a senior studying Computer Science with a minor in Digital Arts and Media at UT. I am from Houston, Texas and I like to draw and do game development in my free time.",
        16,
        "Backend",
        "Phase 2 Leader"
    ),
    new memberInfo(
        "Shreya Sridhar",
        "Shreya Sridhar",
        "Shreya26042000",
        shreya,
        "I am a senior computer science major with a certificate in applied statistical modeling. I was born and raised in Bangalore, India and I have an adorable golden retriever named Sundae back home!",
        11,
        "Backend",
        "Phase 3 Leader"
    ),
    new memberInfo(
        "James Wang",
        "James Wang",
        "JamesCWang",
        james,
        "I am a senior at UT Austin studying Math and Computer Science. I also went to high school in Austin. I like to read books and play video games.",
        32,
        "Frontend",
        "Phase 4 Leader"
    ),
    new memberInfo(
        "McCray Robertson",
        "McCray",
        "m8r",
        mccray,
        "I’m a senior Computer Science major and a minor in business at UT Austin. I’m from Katy, Texas and I enjoy playing video games and running Dungeons & Dragons campaigns in my free time!",
        0,
        "Frontend",
        " "
    )

];

const allToolInfo = [
    new toolInfo("https://reactjs.org/", reactLogo, "react", "React", "Javascript library for frontend development"),
    new toolInfo("https://gitlab.com/", gitlab, "gitlab", "Gitlab", "Git repository and CI/CD platform"),
    new toolInfo("https://discord.com/", discord, "discord", "Discord", "Communication"),
    new toolInfo("https://www.figma.com/", figma, "figma", "Figma", "Design of Frontend"),
    new toolInfo("https://d3js.org/", d3, "d3", "D3", "Javascript visualization library"),
    new toolInfo("https://www.postman.com/", postman, "postman", "Postman", "API Creation"),
    new toolInfo("https://aws.amazon.com/", aws, "aws", "AWS", "Website Hosting"),
    new toolInfo("https://docker.com/", docker, "docker", "Docker", "Containerization for development and testing"),
    new toolInfo("https://namecheap.com/", namecheap, "namecheap", "NameCheap", "Domain name registrar"),
    new toolInfo("https://github.com/psf/black", black, "black", "Black", "Python code formatter"),
    new toolInfo("https://jestjs.io/", jest, "jest", "Jest", "Javascript unit tests"),
    new toolInfo("https://www.selenium.dev/", selenium, "selenium", "Selenium", "GUI unit tests"),
    new toolInfo("https://prettier.io/", prettier, "prettier", "Prettier", "Javascript code formatter"),
    new toolInfo("https://flask.palletsprojects.com/", flask, "flask", "Flask", "Micro web framework"),
    new toolInfo("https://www.sqlalchemy.org/", sqlalchemy, "sqlalchemy", "SQLAlchemy", "SQL toolkit and object-relational mapper"),
    new toolInfo("https://www.mysql.com/", mysql, "mysql", "MySQL", "Relational database management system"),
    new toolInfo("https://docs.python.org/3/library/unittest.html", unittest, "unittest", "unittest", "Unit testing framework"),
    new toolInfo("https://www.canva.com/", canva, "canva", "Canva", "Logo design")
];

const allApiInfo = [
    new apiInfo("https://www.roadgoat.com/business/cities-api", { marginRight:"10%" }, roadgoat, "roadgoat", "Roadgoat", "RESTful API for information on cities"),
    new apiInfo("https://rapidapi.com/apidojo/api/hotels4/", {}, hotels, "hotels.com", "Hotels.com", "RESTful API for information on hotels"),
    new apiInfo("https://opentripmap.io/product", {marginLeft:"10%"}, opentrip, "opentrip", "OpenTripMap", "RESTful API for tourist attractions"),
    new apiInfo("https://openweathermap.org/api",{}, openweather, "openweather", "OpenWeather", "RESTful API for current weather data")
];

function AboutPage() {
    const [ baseUserData ] = useState(allMemberInfo);
    const [ commitData, setCommitData ] = useState(0);
    const [ issueData, setIssueData ] = useState(0);
    const [ unitData ] = useState(64); //****** UPDATE THIS WHEN WE GET UNIT TESTS *********
    
    useEffect(() => {
        const getCommits = async () => {
            await axios.get("https://gitlab.com/api/v4/projects/33876958/repository/contributors")
                .then((response) => response.data)
                .then((data) => {
                    let commits = 0;
                    data.forEach((user) => {
                        const member = baseUserData.find((con) => con.username === user.username || con.username === user.name);
                        if (member !== undefined) {
                            member.commits += user.commits;
                            commits += user.commits;
                        }
                    });
                    setCommitData(commits);
                });
        }
        getCommits();
    }, []);

    useEffect(() => {
        const getIssues = async () => {
            let issues = 0;
            Promise.all(baseUserData.map(async (user) => {
                await axios.get("https://gitlab.com/api/v4/projects/33876958/issues_statistics?author_username=" + user.issueUsername)
                    .then((response) => response.data)
                    .then((data) => {
                        user.issues = data.statistics.counts.all;
                        issues += user.issues;
                    });
            })).then(() => {
                setIssueData(issues);
            });
        }
        getIssues();
    }, []);



    return (
        <Container>
        
         {/* About Heading  */}
         <Row>
             <Col>
                <Card.Text className= "pageTitle"> About Us </Card.Text>
                <Card.Text className="subtitle"> We provide tourists with information about cities and places around the world. We want 
                to give back to the places we visit so users can learn about community events and local businesses, so that tourists can
                give back and support small businesses. This will include everything from cultural festivals to highlighting local 
                restaurants.With globalization on the rise, the world has become so interconnected and we want to provide information to our 
                users- whether they are looking to book a flight tomorrow or just want to “travel” while sitting on the couch- we are here to help!
                </Card.Text>
             </Col>
        </Row> 

         {/* Members Info  */}  
        <Row className='cardSpace'>
            {baseUserData.map((data) => (
                <Col>
                    <MemberCard data={data} />
                </Col>
            ))}
        </Row>

         {/* Gitlab Info  */}
        <Row>
            <Col>
                <Card className="cardStyle" style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Title style={{textAlign: "center"}}> Total Commits</Card.Title>
                        <Card.Text style={{fontSize: 60, textAlign: "center"}} >
                            {commitData}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col>
                <Card className="cardStyle" style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Title style={{textAlign: "center"}}> Total Issues</Card.Title>
                        <Card.Text style={{fontSize: 60, textAlign: "center"}} >
                           {issueData}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col>
                <Card className="cardStyle" style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Title style={{textAlign: "center"}}> Total Unit Tests</Card.Title>
                        <Card.Text style={{fontSize: 60, textAlign: "center"}} >
                            {unitData}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>

         {/* Tools Used  */}
        <Row className='cardSpace'>
            {allToolInfo.map((data) => (
                <Col>
                    <ToolCard data={data} />
                </Col>
            ))}
        </Row>

        {/* Interesting Data  */}
        <Row>
             <Col>
                <Card.Text className= "pageTitle" style={{ fontSize: "35px", marginBottom:"0px" }}> Interesting Data Result </Card.Text>
                <Card.Text className='subtitle'>Our project will allow customers to see information about different cities, including population, geographical information, safety and budget rating and so on.
They can also look for hotels in different cities and view the amenities and general prices of these hotels, as well as interesting activities to do around the city.
There may be noticeable correlations between the population of a city and the safety rating, as well as the budget rating and the prices of hotels and other activities in the area.
This will be a great resource for travelers to consult before planning their  vacations, and will have the added benefit of promoting local vendors and community events in the area.</Card.Text>
             </Col>
        </Row> 

        {/* APIs Used  */}
        <Row>
            <Card.Text className= "pageTitle" style={{ fontSize: "35px", marginTop: "10px", marginBottom:"-30px" }}> APIs </Card.Text>
            {allApiInfo.map((data) => (
                <Col>
                    <ApiCard data={data} />
                </Col>
            ))}
        </Row>

        {/* Gitlab Repo and Postman  */}
        <Row>
            <Card.Text className= "pageTitle" style={{ fontSize: "35px", marginTop: "10px", marginBottom:"-30px" }}> GitLab Repository and Postman API </Card.Text>
            <Card className="toolsStyle" onClick={e => window.open("https://gitlab.com/cs-swe-11am-group-10/going-places")} style={{width: '15rem', cursor: 'pointer'}}>
                <Card.Img src={gitlab} alt="GitLab Repository" />
            </Card>
            <Card className="toolsStyle" onClick={e => window.open("https://documenter.getpostman.com/view/19758941/UVksME8i")} style={{width: '15rem', cursor: 'pointer'}}>
                <Card.Img src={postman} alt="Postman API" />
            </Card>
        </Row>

        </Container>
    );
}

export default AboutPage;