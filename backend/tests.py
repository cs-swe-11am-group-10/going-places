import unittest  # main, TestCase
import json
import urllib
import requests  # for api requests

base_route = "http://api.goingtoplaces.me/"


class UnitTests(unittest.TestCase):
    # Shreya's tests:
    def test_basic(self):
        url = base_route
        r = urllib.request.urlopen(url)
        data = r.read().decode("utf-8")
        self.assertEqual(
            data,
            "Going places API, see https://documenter.getpostman.com/view/19758941/UVypzxKB for documentation",
        )

    def test_get_cities(self):
        url = base_route + "cities"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(len(data), 119)
        self.assertEqual(data[0]["name"], "Albuquerque")
        self.assertEqual(data[0]["id"], 3)

    def test_get_hotels(self):
        url = base_route + "hotels"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(len(data), 227)
        self.assertEqual(data[0]["name"], "The Hotel Blue")
        self.assertEqual(data[0]["id"], 3)

    def test_get_events(self):
        url = base_route + "events"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(len(data), 269)
        self.assertEqual(data[0]["name"], "Barelas")
        self.assertEqual(data[0]["id"], 4)

    def test_get_city_with_id(self):
        url = base_route + "cities/3"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["name"], "Albuquerque")
        self.assertEqual(data["id"], 3)

    def test_get_hotel_with_id(self):
        url = base_route + "hotels/3"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["name"], "The Hotel Blue")
        self.assertEqual(data["id"], 3)

    def test_get_event_with_id(self):
        url = base_route + "events/4"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["name"], "Barelas")
        self.assertEqual(data["id"], 4)

    def test_pagination(self):
        url = base_route + "events?page_size=10&page=1"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(len(data), 10)

    def test_searching(self):
        url = base_route + "cities?search=California"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(len(data), 11)

    def test_filtering(self):
        url = base_route + "cities?name=Anaheim"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data[0]["name"], "Anaheim")

    def test_sorting(self):
        url = base_route + "hotels?sort=city_name-D"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data[0]["city_name"], "Woodinville")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
