from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey


from init import db


class City(db.Model):
    __tablename__ = "city"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    long_name = db.Column(db.String(70))
    api_id = db.Column(db.Integer)
    alternate_names = db.Column(db.Text)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    avg_rating = db.Column(db.Float)
    budget_value = db.Column(db.Float)
    safety_value = db.Column(db.Float)
    state = db.Column(db.String(50))
    country = db.Column(db.String(50))
    continent = db.Column(db.String(50))
    known_for = db.Column(db.Text)
    population = db.Column(db.Integer)
    airbnb_link = db.Column(db.Text)
    google_events_link = db.Column(db.Text)
    wikipedia_link = db.Column(db.Text)
    walk_score_link = db.Column(db.Text)
    image_url = db.Column(db.Text)
    info_text = db.Column(db.Text)

    hotels = relationship("Hotel")
    events = relationship("Event")


class Hotel(db.Model):
    __tablename__ = "hotel"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    api_id = db.Column(db.Integer)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    location_name = db.Column(db.String(50))
    address_line1 = db.Column(db.String(50))
    province_name = db.Column(db.String(50))
    city_name = db.Column(db.String(50))
    country_name = db.Column(db.String(50))
    postal_code = db.Column(db.Integer)
    image_url = db.Column(db.Text)
    room_image_url = db.Column(db.Text)
    map_link = db.Column(db.Text)
    info_text = db.Column(db.Text)
    main_amenities = db.Column(db.Text)
    for_families = db.Column(db.Text)
    star_rating = db.Column(db.Float)
    guest_rating = db.Column(db.Float)
    num_ratings = db.Column(db.Integer)
    price = db.Column(db.Float)
    pets = db.Column(db.Text)
    internet = db.Column(db.Text)

    hotel_city_id = db.Column(db.Integer, ForeignKey("city.id"))
    hotel_city = relationship("City", back_populates="hotels")


class Event(db.Model):
    __tablename__ = "event"

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(50))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    address_line1 = db.Column(db.Text)
    county = db.Column(db.String(40))
    postal_code = db.Column(db.String(20))
    state_name = db.Column(db.String(50))
    city_name = db.Column(db.String(50))
    country_name = db.Column(db.String(50))
    rating = db.Column(db.Float)
    map_link = db.Column(db.Text)
    primary_kind = db.Column(db.String(10))
    kinds = db.Column(db.Text)
    info_text = db.Column(db.Text)
    wikipedia_title = db.Column(db.Text)
    wikipedia_link = db.Column(db.Text)
    image_url = db.Column(db.Text)
    image_url2 = db.Column(db.Text)

    event_city_id = db.Column(db.Integer, ForeignKey("city.id"))
    event_city = relationship("City", back_populates="events")
