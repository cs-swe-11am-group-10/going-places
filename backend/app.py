# main file for the flask app
# runs on http://127.0.0.1:5000/

# Referenced Code

# Politistock
# https://gitlab.com/tiagorossig/politistock/-/blob/dev/backend/docker-flask-mysql/app/app.py

# Bookipedia
# https://gitlab.com/mayankdaruka/bookipedia/-/blob/main/backend/app.py


from typing import List, Dict
from flask import Flask, jsonify, request

# import mysql.connector
from init import app, db
from flask_cors import CORS
from flaskext.mysql import MySQL
import mysql.connector
import json

# configure the database
config = {
    "user": "root",
    "password": "password",
    "host": "goingplaces-restored.c4edpzh9ve9n.us-east-1.rds.amazonaws.com",
    "port": "3306",
    "database": "sys",
}


def get_all(select_statement) -> list:
    """
    Runs select_statement on the database
    Returns result as a jsonified string

    """
    connection = mysql.connector.connect(**config)
    cur = connection.cursor(dictionary=True)
    cur.execute(select_statement)

    result = []  # a list
    for row in cur:
        result.append(row)

    cur.close()
    connection.close()

    return jsonify(result)


def get_all_paginate(page_number, limit, select_statement) -> list:
    """
    Runs select_statement on the database
    Returns result as a jsonified string

    Uses pagination to limit to first 10 results
    unless otherwise passed in by page_number and limit

    """
    connection = mysql.connector.connect(**config)
    cur = connection.cursor(dictionary=True)

    offset = (page_number - 1) * limit
    select_statement += " LIMIT %s OFFSET %s" % (limit, offset)  # pagination

    print(f"paginate query {select_statement}")
    cur.execute(select_statement)

    result = []  # a list
    for row in cur:
        result.append(row)

    cur.close()
    connection.close()

    return jsonify(result)


# uses SQL natural language mode to match search to specified table
# will return the correct query
def search(table, query_value):

    city_cols = ['name', 'state', 'known_for']
    hotel_cols = ['name', 'province_name', 'city_name', 'main_amenities']
    event_cols = ['name', 'state_name', 'city_name', 'primary_kind', 'kinds']

    cols = event_cols
    if table == "city":
        cols = city_cols
    elif table == "hotel":
        cols = hotel_cols

    query = ""
    # now we can match with natural language mode
    # this searches all columns for matches
    if search != "None":
        query = f" WHERE {cols[0]} LIKE ('%{query_value}%')"
    for col in cols[1:]:
        query += f" OR {col} LIKE ('%{query_value}%')"
    return query


def eval_query(queries, sorts, filters, table):

    # page_number = 1
    page_size = int(queries["page_size"][0]) if "page_size" in queries else 10
    page_number = int(queries["page"][0]) if "page" in queries else 1

    select_statement = "SELECT * FROM %s" % table

    query_index = -1
    for query in queries:

        query_index += 1
        print(f"query = {query}")
        print(f"filters for {table} = {filters}")
        query_value = queries[query][0]

        # filter query

        if query in filters:
            lower_bound = None
            upper_bound = None

            # if it is not the first clause in query, use and instead of where
            clause = "WHERE" if query_index == 0 else "AND"
            bounds = query_value.split("-")
            # only one bound specified, treat as equal
            if len(bounds) == 1:
                print(f"length = 1, bound is {bounds[0]}")
                if bounds[0].isnumeric():
                    select_statement += f" {clause} {query} = {bounds[0]}"
                else:
                    select_statement += f" {clause} {query} = '{bounds[0]}'"

            # upper and lower specified
            elif len(bounds) == 2:
                lower_bound = bounds[0]
                upper_bound = bounds[1]

                select_statement += f" {clause} {query} BETWEEN"
                # if the bounds are strings, use character
                # will return the resutls that start with those letters

                if lower_bound.isnumeric():
                    select_statement += f" {lower_bound}"
                else:
                    select_statement += f" '{lower_bound[0]}'"
                select_statement += " AND "

                if upper_bound.isnumeric():
                    select_statement += f" {upper_bound}"
                else:
                    select_statement += f" '{upper_bound[0]}'"
            # user error
            else:
                return "you must specify a bound"

        # sort query - must be last
        elif query == "sort":
            print("in sort")
            # sort=sort_attr-order
            [sort_attr, order] = query_value.split("-")
            select_statement += f" ORDER BY {sort_attr}"
            if order == "D":
                select_statement += " DESC"

        elif query == "search":
            select_statement += search(table, query_value)
    
    #Debugging
    #print(f"query to db -> {select_statement}")

    # explicit pagination
    if "page" in queries:
        return get_all_paginate(page_number, page_size, select_statement)
    # no pagination, return all
    else:
        return get_all(select_statement)


@app.route("/")
def index() -> str:
    return "Going places API, see https://documenter.getpostman.com/view/19758941/UVypzxKB for documentation"


@app.route("/cities", methods=["get"])
def get_cities() -> list:
    """
    Gets a list of all city instances
    """
    queries = request.args.to_dict(flat=False)
    print("query = %s", queries)
    sorts = ["name", "state", "budget_value", "avg_rating", "safety_value"]
    filters = ["budget_value", "avg_rating", "safety_value", "name", "state"]
    return eval_query(queries, sorts, filters, "city")


@app.route("/hotels", methods=["get"])
def get_hotels() -> list:
    """
    Gets a list of all hotel instances
    """
    queries = request.args.to_dict(flat=False)
    sorts = ["name", "city_name", "province_name", "star_rating", "guest_rating"]
    filters = [
        "name",
        "city_name",
        "province_name",
        "star_rating",
        "guest_rating",
        "hotel_city_id",
    ]
    return eval_query(queries, sorts, filters, "hotel")


@app.route("/events", methods=["get"])
def get_events():
    """
    Gets a list of all event instances
    """
    queries = request.args.to_dict(flat=False)
    sorts = ["name", "city_name", "state_name", "rating", "primary_kind", "kinds"]
    filters = [
        "name",
        "city_name",
        "state_name",
        "rating",
        "primary_kind",
        "event_city_id",
        "kinds",
    ]
    return eval_query(queries, sorts, filters, "event")


@app.route("/cities/<id>")
def get_city_with_id(id):
    """
    Returns the city corresponding to the given id
    """
    connection = mysql.connector.connect(**config)
    cur = connection.cursor(dictionary=True)

    select_statement = "SELECT * FROM city WHERE id = %s"

    cur.execute(select_statement, (id,))

    result = []
    # extract row numbers
    for row in cur:
        result.append(row)

    cur.close()
    connection.close()

    return jsonify(result[0])


@app.route("/hotels/<id>")
def get_hotel_with_id(id):
    """
    Returns the hotel corresponding to the given id
    """
    connection = mysql.connector.connect(**config)
    cur = connection.cursor(dictionary=True)

    select_statement = "SELECT * FROM hotel WHERE id = %s"
    cur.execute(select_statement, (id,))

    result = []
    for row in cur:
        result.append(row)

    cur.close()
    connection.close()

    return jsonify(result[0])


@app.route("/events/<id>")
def get_event_with_id(id):
    """
    Returns the event corresponding to the given id
    """
    connection = mysql.connector.connect(**config)
    cur = connection.cursor(dictionary=True)

    select_statement = "SELECT * FROM event WHERE id = %s"
    cur.execute(select_statement, (id,))

    result = []
    for row in cur:
        result.append(row)

    cur.close()
    connection.close()

    return jsonify(result[0])


@app.route("/hotels/city/<cityid>", methods=["get"])
def get_hotel_with_city_id(cityid) -> list:
    """
    Gets a list of all hotel instances corresponding to a certain city
    """
    connection = mysql.connector.connect(**config)
    cur = connection.cursor(dictionary=True)

    cur.execute("SELECT * FROM hotel WHERE hotel_city_id = %s", (cityid,))

    result = []
    for row in cur:
        result.append(row)

    cur.close()
    connection.close()

    return jsonify(result)


@app.route("/events/city/<cityid>", methods=["get"])
def get_event_with_city_id(cityid) -> list:
    """
    Gets a list of all event instances corresponding to a certain city
    """
    connection = mysql.connector.connect(**config)
    cur = connection.cursor(dictionary=True)

    cur.execute("SELECT * FROM event WHERE event_city_id = %s", (cityid,))

    result = []
    for row in cur:
        result.append(row)

    cur.close()
    connection.close()

    return jsonify(result)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
