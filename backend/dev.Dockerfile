# # Use an official Python runtime as an image
# FROM python:3.6

# # The EXPOSE instruction indicates the ports on which a container
# EXPOSE 5000

# # Sets the working directory for following COPY and CMD instructions
# # Notice we haven’t created a directory by this name - this instruction
# # creates a directory with this name if it doesn’t exist
# WORKDIR /app

# COPY requirements.txt /app
# RUN python -m pip install --upgrade pip
# RUN pip install --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host=files.pythonhosted.org --no-cache-dir -r requirements.txt

# # Run app.py when the container launches
# COPY app.py /app
# CMD python app.py



FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive

COPY ./badproxy /etc/apt/apt.conf.d/99fixbadproxy

RUN apt-get clean && apt-get update
RUN apt-get install -y python3
RUN apt-get install -y python3-pip python3-dev build-essential vim

COPY . usr/src/backend
COPY requirements.txt usr/src/backend/requirements.txt

WORKDIR /usr/src/backend

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

EXPOSE 5000

CMD ["python3", "app.py"]
