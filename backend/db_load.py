from http.client import responses
from models import City
from sqlalchemy import select
import json
import os
from dotenv import load_dotenv
from init import db
from models import City, Hotel, Event
import requests
from requests.auth import HTTPBasicAuth
import wikipedia

# 15-20 km
load_dotenv()
ROADGOAT_USER = os.getenv("ROADGOAT_USER")
ROADGOAT_PASS = os.getenv("ROADGOAT_PASS")
RAPID_API_HOST = os.getenv("RAPID_API_HOST")
RAPID_API_KEY = os.getenv("RAPID_API_KEY")
RAPID_API_USER = os.getenv("RAPID_API_USER")
RAPID_API_PASS = os.getenv("RAPID_API_PASS")
CITY_API_KEY = os.getenv("CITY_API_KEY")
BASIC = HTTPBasicAuth(ROADGOAT_USER, ROADGOAT_PASS)
WIKI_REQUEST = "http://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles="


def get_wiki_image(search_term):
    try:
        result = wikipedia.search(search_term, results=1)
        wikipedia.set_lang("en")
        wkpage = wikipedia.WikipediaPage(title=result[0])
        title = wkpage.title
        response = requests.get(WIKI_REQUEST + title)
        json_data = json.loads(response.text)
        img_link = list(json_data["query"]["pages"].values())[0]["original"]["source"]
        return img_link
    except:
        return 0


def reset_db():
    db.session.remove()
    db.drop_all()
    db.create_all()
    print("Database reset")


def get_cities():
    # Later parsed this even further by removing all cities with avg_rating 0, budget/safety value less than 0
    # Get the slugs for the cities using autocomplete API
    with open("data/cities_list.txt") as f:
        cities = f.readlines()

    # Get the slugs of each city:
    with open("data/city_slugs.txt", "w") as f:
        for city in cities:
            city_string = city.strip() + " US"
            print(city_string)
            res = requests.get(
                f"https://api.roadgoat.com/api/v2/destinations/auto_complete?q={city_string}",
                auth=BASIC,
            )
            data = res.json()["data"]
            if len(data) != 0:
                f.write(res.json()["data"][0]["id"])
                f.write("\n")


# Add the city instances to the db:
def add_cities():
    with open("data/city_slugs.txt") as f:
        ids = f.readlines()

    ids = list(map(str.strip, ids))

    with open("data/city_instances.txt", "r") as f:
        cities = f.readlines()

    count = 0
    for city in cities:
        city_json = json.loads(city)
        try:
            city_data = city_json["data"]
            attributes = city_data["attributes"]
            ci = {}
            name, state, country = attributes["long_name"].split(", ")
            ci["name"] = attributes["short_name"]
            long_name = attributes["name"]
            ci["long_name"] = long_name
            ci["api_id"] = city_data["id"]

            if attributes["alternate_names"] == None:
                ci["alternate_names"] = ""
            else:
                ci["alternate_names"] = ",".join(attributes["alternate_names"])

            ci["latitude"] = attributes["latitude"]
            ci["longitude"] = attributes["longitude"]
            ci["avg_rating"] = attributes["average_rating"]
            # Scale of 1 to 8, with 8 being the most expensive
            # Parsed to a scale of 1 to 10, negative values are null
            try:
                budget = attributes["budget"][long_name]["value"]
            except:
                try:
                    budget = attributes["budget"][state]["value"]
                except:
                    budget = -1.0

            ci["budget_value"] = ((budget - 1) * (9.0 / 7.0)) + 1
            # Scale of 1 to 5, with 5 being the safest
            # Parsed to a scale of 1 to 10, negative values are null
            try:
                safety = attributes["safety"][long_name]["value"]
            except:
                try:
                    safety = attributes["safety"][state]["value"]
                except:
                    safety = -1.0
            ci["safety_value"] = ((safety - 1) * (9.0 / 4.0)) + 1

            ci["state"] = state
            ci["country"] = country
            ci["continent"] = "North America"
            known_for = []
            for e in city_json["included"]:
                if e["type"] == "known_for":
                    known_for.append(e["attributes"]["name"])
            ci["known_for"] = ",".join(known_for)
            ci["population"] = attributes["population"]
            ci["airbnb_link"] = attributes["airbnb_url"]
            ci["google_events_link"] = attributes["google_events_url"]
            ci["wikipedia_link"] = attributes["wikipedia_url"]
            ci["walk_score_link"] = attributes["walk_score_url"]
            ci["image_url"] = get_wiki_image(long_name)
            ci["info_text"] = wikipedia.summary(long_name)
            city_instance = City(**ci)
            db.session.add(city_instance)
            count += 1
            if count % 50 == 0:
                db.session.commit()
        except:
            print("Error occured with city: ", id)

    print("DB added, soon to be committed")
    db.session.commit()
    print("Done.")


def get_hotel_destination_ids():
    cities = db.session.query(City).order_by(City.id)
    for city in cities:
        city_name = city.long_name
        city_id = city.id

        headers = {
            "hotels4.p.rapidapi.com": RAPID_API_HOST,
            "x-rapidapi-key": RAPID_API_KEY,
        }
        res = requests.get(
            f"https://hotels4.p.rapidapi.com/locations/v2/search?query={city_name}",
            headers=headers,
        )
        with open("data/hotel_destination_ids.txt", "a") as f:
            f.write(str(city_id))
            f.write("\n")
            f.write(json.dumps(res.json()))
            f.write("\n")


def get_hotel_info():
    with open("data/hotel_destination_ids.txt", "r") as f:
        arr = f.readlines()
    city_ids = list(map(str.strip, arr[0::2]))
    city_jsons = list(map(str.strip, arr[1::2]))
    destinations_ids = []
    for cj in city_jsons:
        city = json.loads(cj)
        id = city["suggestions"][0]["entities"][0]["destinationId"]
        destinations_ids.append(id)

    i = 0
    # print(destinations_ids[48:])
    # print(city_ids[48])
    for id in destinations_ids:
        headers = {
            "hotels4.p.rapidapi.com": RAPID_API_HOST,
            "x-rapidapi-key": RAPID_API_KEY,
        }
        res = requests.get(
            f"https://hotels4.p.rapidapi.com/properties/list?destinationId={id}&pageNumber=1&pageSize=5&checkIn=2022-02-28&checkOut=2022-03-05&adults1=1&sortOrder=GUEST_RATING",
            headers=headers,
        )
        with open("data/hotel_list.txt", "a") as f:
            f.write(str(city_ids[i]))
            f.write("\n")
            f.write(json.dumps(res.json()))
            f.write("\n")
        i += 1


def get_hotels():
    with open("data/hotel_list.txt", "r") as f:
        arr = f.readlines()

    city_ids = list(map(str.strip, arr[0::2]))
    hotel_list = list(map(str.strip, arr[1::2]))

    headers = {
        "hotels4.p.rapidapi.com": RAPID_API_HOST,
        "x-rapidapi-key": RAPID_API_KEY,
    }
    i = 0
    for l in hotel_list:
        hotel_data = json.loads(l)
        result = hotel_data["data"]["body"]["searchResults"]["results"][2]
        id = result["id"]
        res = requests.get(
            f"https://hotels4.p.rapidapi.com/properties/get-details?id={id}",
            headers=headers,
            auth=HTTPBasicAuth(RAPID_API_USER, RAPID_API_PASS),
        )
        with open("data/hotel_instances.txt", "a") as f:
            f.write(str(city_ids[i]))
            f.write("\n")
            f.write(json.dumps(res.json()))
            f.write("\n")
        i += 1


def add_hotels_to_db():
    with open("data/hotel_instances.txt", "r") as f:
        arr = f.readlines()

    city_ids = list(map(str.strip, arr[0::2]))
    hotel_jsons = list(map(str.strip, arr[1::2]))

    i = 0
    for hotel_json in hotel_jsons:
        id = city_ids[i]
        city = db.session.execute(select(City).where(City.id == id)).one()[0]
        hotel = json.loads(hotel_json)
        try:
            data = hotel["data"]
            body = data["body"]
            hi = {}
            name = (
                body["propertyDescription"]["name"]
                + " "
                + body["propertyDescription"]["address"]["cityName"]
            )
            hi["name"] = body["propertyDescription"]["name"]
            hi["api_id"] = body["pdpHeader"]["hotelId"]
            hi["latitude"] = body["pdpHeader"]["hotelLocation"]["coordinates"][
                "latitude"
            ]
            hi["longitude"] = body["pdpHeader"]["hotelLocation"]["coordinates"][
                "longitude"
            ]
            hi["location_name"] = body["pdpHeader"]["hotelLocation"]["locationName"]
            hi["address_line1"] = body["propertyDescription"]["address"]["addressLine1"]
            hi["province_name"] = body["propertyDescription"]["address"]["provinceName"]
            hi["city_name"] = body["propertyDescription"]["address"]["cityName"]
            hi["country_name"] = body["propertyDescription"]["address"]["countryName"]
            hi["postal_code"] = body["propertyDescription"]["address"]["postalCode"]
            hi["image_url"] = get_wiki_image(name)
            hi["map_link"] = ""
            hi["info_text"] = ""

            # Convert this whole list to a string or parse the "title":"Main amenities" one
            amenities = ""
            families = ""
            for e in body["overview"]["overviewSections"]:
                try:
                    if e["title"] == "Main amenities":
                        amenities = ",".join(e["content"])
                    elif e["title"] == "For families":
                        families = ",".join(e["content"])
                except:
                    pass
            hi["main_amenities"] = amenities
            hi["for_families"] = families
            hi["star_rating"] = body["propertyDescription"]["starRating"]
            try:
                hi["guest_rating"] = body["guestReviews"]["brands"]["rating"]
            except:
                hi["guest_rating"] = 10.0
            hi["num_ratings"] = body["guestReviews"]["brands"]["total"]
            try:
                hi["price"] = body["propertyDescription"]["featuredPrice"][
                    "currentPrice"
                ]["plain"]
            except:
                hi["price"] = -1.0
            hi["pets"] = ",".join(
                body["atAGlance"]["travellingOrInternet"]["travelling"]["pets"]
            )
            hi["internet"] = ",".join(
                body["atAGlance"]["travellingOrInternet"]["internet"]
            )
            hotel_instance = Hotel(**hi)
            city.hotels.append(hotel_instance)
            db.session.commit()
        except:
            print(city.name)
        i += 1


def add_images_to_hotels():
    hotels = db.session.query(Hotel).order_by(Hotel.id)
    headers = {
        "hotels4.p.rapidapi.com": RAPID_API_HOST,
        "x-rapidapi-key": RAPID_API_KEY,
    }
    for hotel in hotels:
        api_id = hotel.api_id
        res = requests.get(
            f"https://hotels4.p.rapidapi.com/properties/get-hotel-photos?id={api_id}",
            headers=headers,
        )

        try:
            data = res.json()
            try:
                hotel_image = data["hotelImages"][0]["baseUrl"]
                hotel_image = hotel_image.replace("{size}", "z")
            except:
                hotel_image = ""

            try:
                room_image = data["roomImages"][0]["images"][0]["baseUrl"]
                room_image = room_image.replace("{size}", "z")
            except:
                room_image = ""

            with open("data/hotel_images.txt", "a") as f:
                f.write(str(hotel.id))
                f.write("\n")
                f.write(hotel_image)
                f.write("\n")
                f.write(room_image)
                f.write("\n")

            hotel.image_url = hotel_image
            hotel.room_image_url = room_image
            db.session.commit()
        except:
            with open("data/hotel_images.txt", "a") as f:
                f.write(str(hotel.id))
                f.write("\n")
                f.write(hotel_image)
                f.write("\n")
                f.write(room_image)
                f.write("\n")


def get_xids_of_events():
    # Do one historic, one cultural, one natural per city if possible
    cities = db.session.query(City).order_by(City.id)
    for city in cities:
        city_name = city.long_name
        city_id = city.id
        city_lat = float(city.latitude)
        city_lon = float(city.longitude)

        kinds = ["historic", "cultural", "natural"]
        responses = []
        for kind in kinds:
            res = requests.get(
                f"https://api.opentripmap.com/0.1/en/places/radius?radius=15000&lon={city_lon}&lat={city_lat}&apikey={CITY_API_KEY}&kinds={kind}&limit=10&format=json"
            )
            responses.append(res)

        with open("data/events_xids.txt", "a") as f:
            f.write(str(city_id))
            f.write("\n")
            f.write(json.dumps(responses[0].json()))
            f.write("\n")
            f.write(json.dumps(responses[1].json()))
            f.write("\n")
            f.write(json.dumps(responses[2].json()))
            f.write("\n")


def get_events():
    with open("data/events_xids.txt", "r") as f:
        arr = f.readlines()

    city_ids = list(map(str.strip, arr[0::4]))
    historic_list = list(map(str.strip, arr[1::4]))
    cultural_list = list(map(str.strip, arr[2::4]))
    natural_list = list(map(str.strip, arr[3::4]))

    for i in range(len(city_ids)):
        historic = json.loads(historic_list[i])
        cultural = json.loads(cultural_list[i])
        natural = json.loads(natural_list[i])

        if len(historic) > 0:
            historic_xid = historic[0]["xid"]
        else:
            historic_xid = ""

        if len(cultural) > 0:
            cultural_xid = cultural[0]["xid"]
        else:
            cultural_xid = ""

        if len(natural) > 0:
            natural_xid = natural[0]["xid"]
        else:
            natural_xid = ""

        responses = []
        for xid in [historic_xid, cultural_xid, natural_xid]:
            res = requests.get(
                f"https://api.opentripmap.com/0.1/en/places/xid/{xid}?apikey={CITY_API_KEY}&lang=en"
            )
            responses.append(res)

        with open("data/event_instances.txt", "a") as f:
            f.write(str(city_ids[i]))
            f.write("\n")
            f.write(json.dumps(responses[0].json()))
            f.write("\n")
            f.write(json.dumps(responses[1].json()))
            f.write("\n")
            f.write(json.dumps(responses[2].json()))
            f.write("\n")


def add_events_to_db():
    with open("data/event_instances.txt", "r") as f:
        arr = f.readlines()

    city_ids = list(map(str.strip, arr[0::4]))
    historic_list = list(map(str.strip, arr[1::4]))
    cultural_list = list(map(str.strip, arr[2::4]))
    natural_list = list(map(str.strip, arr[3::4]))

    for i in range(len(city_ids)):
        id = city_ids[i]
        city = db.session.execute(select(City).where(City.id == id)).one()[0]
        arr = [
            json.loads(historic_list[i]),
            json.loads(cultural_list[i]),
            json.loads(natural_list[i]),
        ]
        kinds = ["historic", "cultural", "natural"]
        for j in [0, 1, 2]:
            data = arr[j]
            try:
                ei = {}
                ei["name"] = data["name"]
                ei["latitude"] = data["point"]["lat"]
                ei["longitude"] = data["point"]["lon"]
                try:
                    ei["address_line1"] = data["address"]["road"]
                except:
                    ei["address_line1"] = ""
                try:
                    ei["county"] = data["address"]["county"]
                except:
                    ei["county"] = ""
                ei["postal_code"] = data["address"]["postcode"]
                ei["state_name"] = data["address"]["state"]
                ei["city_name"] = data["address"]["city"]
                ei["country_name"] = data["address"]["country"]
                ei["rating"] = int(data["rate"][0])
                ei["map_link"] = ""
                ei["primary_kind"] = kinds[j]
                ei["kinds"] = data["kinds"]
                ei["info_text"] = data["wikipedia_extracts"]["text"]
                ei["wikipedia_title"] = data["wikipedia_extracts"]["title"]
                ei["wikipedia_link"] = data["wikipedia"]
                ei["image_url"] = data["preview"]["source"]
                ei["image_url2"] = data["image"]

                event_instance = Event(**ei)
                city.events.append(event_instance)
            except:
                pass

        db.session.commit()


def parse_kinds():
    events = db.session.query(Event).order_by(Event.id)
    # official_list = ['interesting_places', 'historic', 'cultural', 'natural', 'museums',
    #     'theatres_and_entertainments', 'urban_environment', 'archaeology',
    #     'burial_places', 'fortifications', 'historical_places',
    #     'monuments_and_memorials', 'beaches', 'geological_formations',
    #     'islands', 'natural_springs', 'nature_reserves', 'water']
    for event in events:
        #     # city_name = city.long_name
        #     # city_id = city.id
        kinds = event.kinds
        #     kind_list = kinds.split(",")
        #     # print(kind_list)
        #     kind_dup = kind_list.copy()
        #     for kind in kind_dup:
        #         # kind = kind_list[i]
        #         if kind in official_list:
        #             # print(kind)
        #             kind_list.remove(kind)
        #     string = ','.join(kind_list)
        #     event.kinds = string
        kind_list = kinds.split("_")
        string = (" ").join(kind_list)
        string = string.capitalize()
        # print(string)
        event.kinds = string
        db.session.commit()


def map_states():
    hotels = db.session.query(Hotel).order_by(Hotel.id)
    with open("data/states_map.json") as json_file:
        state_dict = json.load(json_file)
        for hotel in hotels:
            state_abbr = hotel.province_name
            # print(state_abbr)
            # print(state_dict[state_abbr])
            hotel.province_name = state_dict[state_abbr]
            db.session.commit()


# get_hotel_destination_ids()
# get_hotel_info()
# get_hotels()
# add_hotels_to_db()
# get_xids_of_events()
# get_events()
# add_events_to_db()
# add_images_to_hotels()
# parse_kinds()
map_states()
