## Going Places

#### Group Members (GitLab ID, EID):

Sweta Ghose: swetagh, sg48742  
James Wang: JamesCWang, jcw4325  
McCray Robertson: m8r, mrr3458  
Shreya Sridhar: Shreya26042000, ss86542  
Joriann Bassi: joriann-marie, jmb9632

#### Git SHA:

- Phase 1: e0e9e0031cc881c13b0b02de9efaac02a7650989
- Phase 2: ca7a44ba3b279853dce4fe15aa12967c00135d32
- Phase 3: 13b231f3d35a41fe08dd6c9c6f6ebcee08ff1174
- Phase 4: 09a5b6fb771c507c599bca68a9bf84bcfa98a415

#### Project Leader:

- Phase 1: Sweta Ghose
- Phase 2: Joriann Bassi
- Phase 3: Shreya Sridhar
- Phase 4: James Wang

#### Useful links:

GitLab Pipelines: https://gitlab.com/cs-swe-11am-group-10/going-places/-/pipelines

Website: https://www.goingtoplaces.me/

API Documentation: https://documenter.getpostman.com/view/19758941/UVypzxKB

#### Estimated completion time:

##### Phase 1:

- Sweta - 10 hours
- James - 10 hours
- McCray - 10 hours
- Shreya - 10 hours
- Joriann - 10 hours

##### Phase 2:

- Sweta - 15 hours
- James - 15 hours
- McCray - 15 hours
- Shreya - 15 hours
- Joriann - 15 hours

##### Phase 3:

- Sweta - 15 hours
- James - 15 hours
- McCray - 15 hours
- Shreya - 15 hours
- Joriann - 15 hours

##### Phase 4:

- Sweta - 3 hours
- James - 3 hours
- McCray - 3 hours
- Shreya - 3 hours
- Joriann - 3 hours

#### Actual completion time:

##### Phase 1:

- Sweta - 13 hours
- James - 12 hours
- McCray - 10 hours
- Shreya - 15 hours
- Joriann - 10 hours

##### Phase 2:

- Sweta - 17 hours
- James - 17 hours
- McCray - 17 hours
- Shreya - 17 hours
- Joriann - 20 hours

##### Phase 3:

- Sweta - 17 hours
- James - 16 hours
- McCray - 15 hours
- Shreya - 15 hours
- Joriann - 17 hours

##### Phase 4:

- Sweta - 3 hours
- James - 5 hours
- McCray - 10 hours
- Shreya - 3 hours
- Joriann - 3 hours

#### Presentation:

https://youtu.be/brc0CdO-Sp4

#### Comments:

We referenced previous projects to get an idea of what was expected of us and the ones linked in the rubric.
