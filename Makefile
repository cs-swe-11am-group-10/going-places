.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# All of these make commands must be called in root directory

# run docker
docker:
	docker run -p 5000:5000 swetagh/going-places

# run docker-compose
docker-compose:
	docker-compose up --build --force-recreate

all:

install:
	cd frontend && npm install

run-frontend:
	cd frontend && npm install && npm start

back_run:
	cd backend/ && eb local run --port 5000

tests:
	echo "Running python unit tests..."
	python backend/test.py -v

jest:
	echo "Running frontend jest tests"
	cd frontend && npm test

selenium:
	echo "Running frontend selenium tests"
	python3 frontend/guiTests.py 


docker:
	docker run --rm -i -t -v $(PWD):/usr/python -w /usr/python gpdowning/python

format:
	black backend

install:
	pip install -r ./backend/requirements.txt

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        


# uncomment the following line once you've pushed your test files
# you must replace GitLabID with your GitLabID

# check the existence of check files
check: $(CFILES)

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__

